<?php

namespace Gruniversal\Lib;

/**
 * Simple tool for debugging timing issues
 *
 * @see     https://gitlab.com/gruniversal/redmine-board
 * @license https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de
 * @author  David Gruner (mailto:gruniversal@gmx.de / https://www.gruniversal.de)
 *
 */
class TimeDebug {

    private static $timedebug_init;

    public static $timedebug_log;

    private static $timedebug_diff;

    public static function init() {

        self::$timedebug_init = hrtime( true );
        self::$timedebug_log = [ 0 => "init" ];
        self::$timedebug_diff = 0;
    }

    public static function getDiffElapsed() {

        self::$timedebug_diff = self::getTimeElapsed() - array_key_last( self::$timedebug_log );

        return self::$timedebug_diff;
    }

    public static function getTimeElapsed() {

        return hrtime( true ) - self::$timedebug_init;
    }

    public static function set( string $txt ) {

        $elapsed = self::getTimeElapsed();

        self::$timedebug_diff = $elapsed - array_key_last( self::$timedebug_log );

        self::$timedebug_log[$elapsed] = $txt;
    }

    public static function show() {

        if ( "127.0.0.1" !== $_SERVER['REMOTE_ADDR'] ) {
            return;
        }

        print "<hr><pre>";
        foreach ( self::$timedebug_log as $elapsed => $txt ) {
            $diff = $elapsed - ( $last ?? 0 );
            $last = $elapsed;

            print "[" . self::formatAsSeconds( $elapsed ) . "|" . self::formatAsSeconds( $diff ) . "] " . $txt . "<br>";
        }
    }

    public static function formatAsSeconds( $timestamp ) {

        return sprintf( '%2.8f', $timestamp / 1000000000 );
    }

}
