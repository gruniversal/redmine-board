<?php

/**
 * Display errors and exceptions in DEV environment
 *
 * @see     https://gitlab.com/gruniversal/redmine-board
 * @license https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de
 * @author  David Gruner (mailto:gruniversal@gmx.de / https://www.gruniversal.de)
 *
 */

if ( "127.0.0.1" === $_SERVER['REMOTE_ADDR'] ) {

    ini_set( 'display_errors', 1 );
    ini_set( 'display_startup_errors', 1 );
    error_reporting( E_ALL );

    set_exception_handler(
        function ( $e ) {
            print "<pre style='background: yellow'>" . get_class( $e ) . ": " . $e->getMessage() . "</pre>";
            print "<pre>(thrown in " . $e->getFile() . " on line " . $e->getLine() . ")</pre>";
            print "<pre>" . $e->getTraceAsString() . "</pre>";
        }
    );

} else {

    ini_set( 'display_errors', 0 );
    ini_set( 'display_startup_errors', 0 );
    error_reporting( E_ALL & ~E_NOTICE );

    set_exception_handler(
        function ( $e ) {
            print "<pre style='background: yellow'>" . get_class( $e ) . ": " . $e->getMessage() . "</pre>";
        }
    );

}
