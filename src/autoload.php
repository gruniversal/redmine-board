<?php

spl_autoload_register(
    function ( $class ) {

        $ns_parts = explode( '\\', $class );

        if ( str_starts_with( $class, "RedmineBoard" ) ) {
            $file = __DIR__ . '/' . implode( '/', $ns_parts ) . '.php';
            require_once $file;
        }

        if ( str_starts_with( $class, "Gruniversal\Lib" ) ) {
            $file = __DIR__ . '/' . implode( '/', array_slice( $ns_parts, 1 ) ) . '.php';
            require_once $file;
        }

        return;
    }
);
