<?php

namespace RedmineBoard;

use \Gruniversal\Lib;

/**
 * Two dimensional data array for issues
 *
 * @see     https://gitlab.com/gruniversal/redmine-board
 * @license https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de
 * @author  David Gruner (mailto:gruniversal@gmx.de / https://www.gruniversal.de)
 *
 */
class Matrix {

    // configuration (see: RedmineBoard\Config)
    private $config;

    // name of dimension X (columns)
    private $dimX;

    // name of dimension Y (rows)
    private $dimY;

    // two dimensional array (X,Y) of a list of entries in the matrix
    private $cell;

    // list of columns
    private $columns = [];

    // list of rows
    private $rows = [];

    // list of existing entries for a dimension
    private $list = [];

    // list of counts for calc fields
    private $counts = [];

    // list of sums for calc fields
    private $sums = [];

    /*****************************************************/
    /** GET METHODS
    /*****************************************************/

    public function getDimensionList( string $dimension ) {
        return $this->list[$dimension];
    }

    public function getDimensionLists() {
        return $this->list;
    }

    public function getDimX() {
        return $this->dimX;
    }

    public function getDimY() {
        return $this->dimY;
    }

    public function getColumns() {
        return $this->columns;
    }

    public function getRows() {
        return $this->rows;
    }

    public function getCells() {
        return $this->cell;
    }

    public function getCounts() {
        return $this->counts;
    }

    public function getSums() {
        return $this->sums;
    }

    /*****************************************************/
    /** CONSTRUCTOR
    /*****************************************************/

    /**
     * Constructor
     *
     * @param      object               $config  board configuration (from boards.json)
     *
     * @return     RedmineBoardMatrix
     */
    public function __construct( \RedmineBoard\Config $config ) {

        $this->config = $config;

        // set dimensions
        $this->dimX = $this->config->getColumnsField();
        $this->dimY = $this->config->getRowsField();

        // set predefined values
        foreach ( $this->config->getColumnsValues() as $value ) {
            $this->addDistinctColumn( $value );
        }
        foreach ( $this->config->getRowsValues() as $value ) {
            $this->addDistinctRow( $value );
        }

        return $this;
    }

    /*****************************************************/
    /** DATA MANAGEMENT
    /*****************************************************/

    /**
     * Adds an entry to the matrix
     *
     * @param      RedmineBoardIssue  $entry  The entry
     *
     * @return     self
     */
    public function addEntry( \RedmineBoard\Issue $issue ) {

        // determine column and row
        $column = $issue->getFieldValue( $this->dimX );
        $row = $issue->getFieldValue( $this->dimY );

        // add to columns and rows list if needed
        $this->addDistinctColumn( $column );
        $this->addDistinctRow( $row );

        // collect dimensions for filter lists
        $this->collectDimensions( $issue );

        // add entry to matrix
        $this->cell[$column][$row][] = $issue;

        return $this;
    }

    /**
     * Adds dimensions of a given entry to the matrix (used for filter lists)
     *
     * @param      array  $entry  The entry
     *
     * @return     self
     */
    public function collectDimensions( \RedmineBoard\Issue $issue ) {

        foreach ( $issue->getRedmineDimensions() as $dimension ) {
            $this->addDistinctDimension( $dimension, $issue->getFieldArray( $dimension ) );
        }

        return $this;
    }

    /**
     * Sort all dimension lists
     *
     * @return     self
     */
    public function sortDimensionLists() {

        foreach ( $this->list as &$dimension_list ) {
            uasort(
                $dimension_list,
                function ( $a, $b ) {
                    return strnatcasecmp( $a['name'], $b['name'] );
                }
            );
        }

        return $this;
    }

    /**
     * Sort columns
     *
     * @return     self
     */
    public function sortColumns() {

        natcasesort( $this->columns );

        return $this;
    }

    /**
     * Sort rows
     *
     * @return     self
     */
    public function sortRows() {

        natcasesort( $this->rows );

        return $this;
    }

    /**
     * Adds a dimension entry to the list (if not already present)
     *
     * @param      string  $dim_name   dimension name
     * @param      array   $dimension  dimension
     *
     * @return     self
     */
    private function addDistinctDimension( string $dim_name, array $dimension ) {

        if ( false === isset( $this->list[$dim_name] ) ) {
            $this->list[$dim_name] = [];
        }

        if ( false === in_array( $dimension, $this->list[$dim_name] ) ) {
            $this->list[$dim_name][] = $dimension;
        }

        return $this;
    }

    /**
     * Adds a column to the list (if not already present)
     *
     * @param      string  $name   name of the column
     *
     * @return     self
     */
    private function addDistinctColumn( string $name ) {

        if ( false === in_array( $name, $this->columns ) ) {
            $this->columns[] = $name;
        }

        return $this;
    }

    /**
     * Adds a row to the list (if not already present)
     *
     * @param      string  $name   name of the row
     *
     * @return     self
     */
    private function addDistinctRow( string $name ) {

        if ( false === in_array( $name, $this->rows ) ) {
            $this->rows[] = $name;
        }

        return $this;
    }

    /*****************************************************/
    /** COUNT / SUM
    /*****************************************************/

    /**
     * Calculates the counts and sums.
     *
     * @param      array  $calc_fields
     *
     * @return     self
     */
    public function calculateCountsAndSums( array $calc_fields ) {

        Lib\TimeDebug::set( __METHOD__ );

        // reset counts / sums
        foreach ( $calc_fields as $field => $definition ) {
            $this->counts[$field] = 0;
            $this->sums[$field] = 0;

            foreach ( $this->columns as $x ) {
                $this->counts["column:" . $x][$field] = 0;
                $this->sums["column:" . $x][$field] = 0;
            }
            foreach ( $this->rows as $y ) {
                $this->counts["row:" . $y][$field] = 0;
                $this->sums["row:" . $y][$field] = 0;
            }
        }

        // go through all cells
        foreach ( $this->columns as $x ) {
            foreach ( $this->rows as $y ) {

                if ( true === isset( $this->cell[$x][$y] ) ) {
                    foreach ( $this->cell[$x][$y] as $issue ) {

                        // count and sum for all issues
                        foreach ( $calc_fields as $field => $definition ) {

                            $value = $issue->getFieldValue( $field );

                            if ( isset( $value ) && ( '' !== $value ) ) {
                                $this->counts[$field] += 1;
                                $this->counts["column:" . $x][$field] += 1;
                                $this->counts["row:" . $y][$field] += 1;
                            }
                            if ( isset( $value ) && ( is_numeric( $value ) || is_bool( $value ) ) ) {
                                $this->sums[$field] += $value;
                                $this->sums["column:" . $x][$field] += $value;
                                $this->sums["row:" . $y][$field] += $value;
                            }
                        }
                    }
                }
            }
        }

        Lib\TimeDebug::set( __METHOD__ . " end" );

        return $this;
    }

    /**
     * Counts the number of all entries in the matrix
     *
     * @return     int   number of entries
     */
    public function countEntries() {

        $result = 0;

        foreach ( $this->columns as $x ) {
            foreach ( $this->rows as $y ) {
                if ( true === isset( $this->cell[$x][$y] ) ) {
                    $result += count( $this->cell[$x][$y] );
                }
            }
        }

        return $result;
    }

}
