<?php

namespace RedmineBoard;

use \Gruniversal\Lib;

/**
 * Redmine Board - main controller class
 *
 * @uses    PHP Redmine API (https://github.com/kbsali/php-redmine-api/)
 *
 * @see     https://gitlab.com/gruniversal/redmine-board
 * @license https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de
 * @author  David Gruner (mailto:gruniversal@gmx.de / https://www.gruniversal.de)
 *
 */
class Main {

    // displayed version
    private $version = "v1.6.1";

    // global configuration (see: RedmineBoardGlobalConfig.php)
    private $config = [];

    // boards configuration (see: boards.json)
    private $boards = [];

    // template engine
    private $twig = null;

    // redmine connection instance
    private $redmine = null;

    // list of known and filtered GET params
    private $params = [];

    // list of configured filters
    private $filter = [];

    // list of applied filters (those with non-empty values)
    private $applied_filters = [];

    // id of active board
    private $board_id = 1;

    // configuration of active board
    private $board_config = null;

    // contains a two dimensional data object for issues
    private $matrix = null;

    // list of twig templates and values to render
    private $templates_to_render = [];

    /*****************************************************/
    /** INITIALIZATION
    /*****************************************************/

    /**
     * Simple constructor
     *
     * @return     self
     */
    public static function create() {
        return new self();
    }

    /**
     * Read global config (RedmineBoardGlobalConfig.php) and boards config (boards.json)
     *
     * @throws     ConfigurationError
     *
     * @return     self
     */
    private function loadConfig() {

        Lib\TimeDebug::set( __METHOD__ );

        // load global configuration
        $this->config = \RedmineBoardGlobalConfig::getConfig();

        // load boards configuration
        $boards_config_file = $this->config['boards']['config'];
        if ( false === is_readable( $boards_config_file ) ) {
            throw new Lib\ConfigurationError( "boards config file not found" );
        }
        $this->boards = json_decode( file_get_contents( $boards_config_file ), true );
        if ( null === $this->boards ) {
            throw new Lib\ConfigurationError( "boards config file is no proper json file" );
        }

        return $this;
    }

    /*****************************************************/
    /** PARAMS
    /*****************************************************/

    /**
     * Process GET parameters
     *
     * @param      array               $params
     *
     * @throws     ConfigurationError
     *
     * @return     self
     */
    private function processParams( array $params ) {

        Lib\TimeDebug::set( __METHOD__ );

        // safe params for link generation
        $this->params = [];

        // hold active filters
        $this->filter = [];

        // filter for known params
        foreach ( $params as $key => $value ) {
            switch ( $key ) {

                case 'category':    // filter category
                    $this->filter[$key] = $params[$key];
                    $this->params[$key] = $params[$key];
                    break;

                case 'assigned_to': // filter assignee
                case 'project':     // filter project
                case 'author':      // filter author
                case 'status':      // filter status
                case 'priority':    // filter priority
                case 'tracker':     // filter tracker
                    $this->filter[$key] = intval( $params[$key] );
                    $this->params[$key] = intval( $params[$key] );
                    break;

                case 'board':       // select board
                    $this->params[$key] = intval( $params[$key] );
                    break;

                case 'refresh':     // trigger cache refresh
                    // $this->config['redmine']['cache']['seconds'] = 0;
                    break;

                case 'search':      // filter search
                    $this->params[$key] = $value;
                    break;

                default:
                    throw new Lib\ConfigurationError( "unknown GET parameter '" . $key . "'" );
            }
        }

        // build list of applied filters (apply only non-empty filters)
        $this->applied_filters = [];
        foreach ( $this->filter as $key => $value ) {
            if ( ! empty( $value ) ) {
                $this->applied_filters[$key] = $value;
            }
        }

        return $this;
    }

    /*****************************************************/
    /** BOARD MATRIX
    /*****************************************************/

    /**
     * Setup board configuration and initiate matrix
     *
     * @throws     ConfigurationError
     *
     * @return     self
     */
    private function setupBoardMatrix() {

        Lib\TimeDebug::set( __METHOD__ );

        // set active board by: 1) url parameter, 2) config boards.default, 3) first in board.json
        $this->board_id = intval( $this->params['board'] ?? ( $this->config['boards']['default'] ?? 1 ) );

        // assure board has configuration
        if ( false === isset( $this->boards[ $this->board_id ] ) ) {
            throw new Lib\ConfigurationError( "configuration for board " . $this->board_id . " not found" );
        }

        // check and setup board configuration
        $this->board_config = new Config( $this->boards[ $this->board_id ] );

        // init matrix
        $this->matrix = new Matrix( $this->board_config );

        return $this;
    }

    /**
     * Retrieve issues from redmine and add them to the matrix
     *
     * @return     self
     */
    private function getMatrixIssuesFromRedmine() {

        Lib\TimeDebug::set( __METHOD__ );

        $this->redmine = new Connection( $this->config['redmine'] );

        foreach ( $this->redmine->getBoardIssues(
            [
                'tracker_id'     => $this->board_config->getTrackers() ?? '',
                'project_id'     => $this->board_config->getProjects() ?? '',
                'assigned_to_id' => $this->board_config->getAssignees() ?? ''
            ]
        ) as $raw_data ) {

            $issue = new Issue( $raw_data );

            // perform some data operations to all issues
            $issue->applyDataModifiers( $this->board_config->getModifiers() );

            // check if issue is filtered
            if ( true === $issue->matchesFilter( $this->applied_filters ) ) {
                // add extra information to the issue
                $issue->calculateAdditionalFields( $this->board_config->getCalculations() );
                $issue->setStyleInformation( $this->board_config->getStyles() );
                // add to matrix
                $this->matrix->addEntry( $issue );
            } else {
                // filtered: add only to dimension lists
                $this->matrix->collectDimensions( $issue );
            }
        }

        return $this;
    }

    /**
     * Adds additional board information and does sorting
     *
     * @return     self
     */
    public function completeMatrixData() {

        Lib\TimeDebug::set( __METHOD__ );

        // sort columns / rows
        if ( true === $this->board_config->getColumnsSort() ) {
            $this->matrix->sortColumns();
        }
        if ( true === $this->board_config->getRowsSort() ) {
            $this->matrix->sortRows();
        }

        // sort filter lists
        $this->matrix->sortDimensionLists();

        // calculate counts and sums
        $this->matrix->calculateCountsAndSums( $this->board_config->getCalculations() );

        return $this;
    }

    /*****************************************************/
    /** OUTPUT
    /*****************************************************/

    /**
     * Setup template engine
     *
     * @return     self
     */
    private function setupTemplateEngine() {

        Lib\TimeDebug::set( __METHOD__ );

        $options = [];

        // activate twig cache if set in global config
        if ( true === $this->config['twig']['cache']['active'] ) {
            $options['cache'] = $this->config['twig']['cache']['path'];
            $options['auto_reload'] = true;
        }

        // activate twig debug mode if set in global config
        if ( true === $this->config['debug'] ) {
            $options['debug'] = true;
        }

        // load templates from theme folder
        $loader = new \Twig\Loader\FilesystemLoader( REDMINEBOARD_THEMES_DIR . $this->config['theme'] . '/twig/' );
        $this->twig = new \Twig\Environment( $loader, $options );

        // activate twig debug extension if set in global config
        if ( true === $this->config['debug'] ) {
            $this->twig->addExtension( new \Twig\Extension\DebugExtension() );
        }

        return $this;
    }

    /**
     * Set up templates and values for template engine
     *
     * @return     self
     */
    private function prepareTemplates() {

        Lib\TimeDebug::set( __METHOD__ );

        // these sections are rendered with the corresponding values
        $layout = [

            "header" => [
                'url'           => $this->config['redmine']['connection']['server'],
                'title'         => $this->board_config->getName()
            ],

            "headline" => [
                'title'         => $this->board_config->getName(),
                'boards'        => $this->boards,
                'current_id'    => $this->board_id
            ],

            "filter" => [
                'filters'       => $this->board_config->getFilters(),
                'params'        => $this->params,
                'dimensions'    => $this->matrix->getDimensionLists(),
                'search'        => $this->params['search']
            ],

            "board" => [
                'dimX'          => $this->matrix->getDimX(),
                'dimY'          => $this->matrix->getDimY(),
                'columns'       => $this->matrix->getColumns(),
                'rows'          => $this->matrix->getRows(),
                'cells'         => $this->matrix->getCells(),
                'sums'          => $this->matrix->getSums(),
                'counts'        => $this->matrix->getCounts(),
                'url'           => $this->config['redmine']['connection']['server']
            ],

            "subline" => [
                'count_issues'      => $this->matrix->countEntries(),
                'count_issues_all'  => $this->redmine->getCount(),
                'limit'             => intval( $this->config['redmine']['connection']['limit'] ?? 0 ),
                'timestamp'         => $this->redmine->getTimestamp(),
                'minutes'           => intval( ( time() - $this->redmine->getTimestamp() ) / 60 ),
                'refresh_url'       => $this->createUrl( ['refresh' => 1] )
            ],

            "custom" => [
                'sums'              => $this->matrix->getSums(),
                'counts'            => $this->matrix->getCounts(),
                'count_issues'      => $this->matrix->countEntries(),
                'count_issues_all'  => $this->redmine->getCount(),
            ],

            "footer" => [
                'version'           => $this->version
            ]
        ];

        // if configured render also the debug box
        if ( true === $this->config['debug'] ) {
            $layout['debug'] = [
                'config'    => $this->config,
                'board'     => $this->board_config->getConfig(),
                'params'    => $this->params
            ];
        }

        // if configured replace with custom templates (else: use default templates)
        $custom_templates = $this->board_config->getTemplates();

        foreach ( $layout as $template => $values ) {
            if ( true === isset( $custom_templates[$template] ) ) {
                $location = $custom_templates[$template];
            } else {
                $location = $template . ".twig";
            }
            $this->templates_to_render[$location] = $values;
        }

        return $this;
    }

    /**
     * Render and display the board
     *
     * @return     true
     */
    private function showBoard() {

        Lib\TimeDebug::set( __METHOD__ );

        $html = '';

        foreach ( $this->templates_to_render as $template => $values ) {

            Lib\TimeDebug::set( __METHOD__ . " $template" );

            $html .= $this->twig->render( $template, $values );
        }

        Lib\TimeDebug::set( __METHOD__ . " output" );

        print $html;

        return true;
    }

    /**
     * Create an url based on collected and given parameters
     *
     * @param      array   $params
     *
     * @return     string url
     */
    private function createUrl( array $params = [] ) {

        $url_scheme = "http" . ( ! empty( $_SERVER['HTTPS'] ) ? "s" : "" ) . "://";
        $url_params = http_build_query( array_merge( $this->params, $params ) );

        $url = $url_scheme . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'] . "?" . $url_params;

        return $url;
    }

    /*****************************************************/
    /** SILENT REFRESH
    /*****************************************************/

    /**
     * Silently refresh board data if updated (since last cache timestamp)
     *
     * @return     self
     */
    private function silentRefresh() {

        if ( true !== $this->config['redmine']['cache']['active'] ) {
            print "redmine cache needs to be active to use silent refresh";
            return $this;
        }

        // set active board by: 1) url parameter, 2) config boards.default, 3) first in board.json
        $board_id = intval( $_GET['board'] ?? ( $this->config['boards']['default'] ?? 1 ) );

        // assure board has configuration
        if ( false === isset( $this->boards[ $board_id ] ) ) {
            throw new Lib\ConfigurationError( "configuration for board " . $board_id . " not found" );
        }

        // check and setup board configuration
        $this->board_config = new Config( $this->boards[ $board_id ] );

        // refresh board data if cache is outdated (older than config: redmine.cache.seconds)
        $this->redmine = new Connection( $this->config['redmine'] );
        $count = $this->redmine->refreshBoardIssues(
            [
                'tracker_id'     => $this->board_config->getTrackers() ?? '',
                'project_id'     => $this->board_config->getProjects() ?? '',
                'assigned_to_id' => $this->board_config->getAssignees() ?? ''
            ]
        );

        print "$count issues refreshed in " . Lib\TimeDebug::formatAsSeconds( Lib\TimeDebug::getTimeElapsed() ) . "s";

        return $this;
    }

    /*****************************************************/
    /** REFRESH
    /*****************************************************/

    /**
     * Send a JavaScript snippet to reload the whole website
     */
    private function reloadApplication() {

        print "<script>window.location.replace('" . $this->createUrl() . "');</script>";

        return true;
    }

    /*****************************************************/
    /** RUN METHOD
    /*****************************************************/

    /**
     * Static call to run the application
     *
     * @return     true
     */
    public static function run() {

        // TODO: there should be a better way to do this

        // refresh board data: perform silentrefresh and reload
        if ( isset( $_GET['refresh'] ) ) {
            self::create()
                ->loadConfig()
                ->processParams( $_GET )
                ->silentRefresh()
                ->reloadApplication();
            return true;
        }

        // silently refresh board data if outdated
        if ( isset( $_GET['silentrefresh'] ) ) {
            self::create()
                ->loadConfig()
                ->silentRefresh();
            return true;
        }

        self::create()
            ->loadConfig()
            ->processParams( $_GET )
            ->setupBoardMatrix()
            ->getMatrixIssuesFromRedmine()
            ->completeMatrixData()
            ->setupTemplateEngine()
            ->prepareTemplates()
            ->showBoard();

        return true;
    }

}
