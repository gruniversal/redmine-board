<?php

namespace RedmineBoard;

/**
 * This class hold issue data and some operations with it
 *
 * @see     https://gitlab.com/gruniversal/redmine-board
 * @license https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de
 * @author  David Gruner (mailto:gruniversal@gmx.de / https://www.gruniversal.de)
 *
 */
class Issue {

    // issue data
    private $data;

    // redmine default dimensions
    private static $redmine_dimensions = [
        'project',
        'tracker',
        'status',
        'priority',
        'author',
        'assigned_to',
        'category'
    ];

    /*****************************************************/
    /** GET METHODS
    /*****************************************************/

    /**
     * Get value from original redmine data or calculated field
     *
     * @param      string  $field
     *
     * @return     mixed   field value
     */
    public function getFieldValue( string $field ) {

        // get value from original redmine data or calculated field
        if ( true === isset( $this->data[$field] ) ) {
            $value = $this->data[$field]['name'];
        } elseif ( true === isset( $this->data['_calc'][$field] ) ) {
            $value = $this->data['_calc'][$field];
        } else {
            $value = null;
        }

        return $value;
    }

    public function getFieldId( string $field ) {
        return $this->data[$field]['id'];
    }

    public function getFieldArray( string $field ) {
        return $this->data[$field];
    }

    public static function getRedmineDimensions() {
        return self::$redmine_dimensions;
    }

    /*****************************************************/
    /** MAGIC GET METHODS (for twig)
    /*****************************************************/

    public function __isset( string $field ) {
        return isset( $this->data[$field] );
    }

    public function __get( string $field ) {
        return $this->data[$field];
    }

    /*****************************************************/
    /** CONSTRUCTOR
    /*****************************************************/

    public function __construct( array $raw_data ) {

        // set empty dimensions if not given (fixes php notice)
        foreach ( self::$redmine_dimensions as $dimension ) {
            if ( false === isset( $raw_data[$dimension] ) ) {
                $raw_data[$dimension] = ['id' => -1, 'name' => ''];
            }
        }

        $this->data = $raw_data;

        return $this;
    }

    /*****************************************************/
    /** FILTER
    /*****************************************************/

    /**
     * Checks if the issue matches a given filter array
     *
     * @param      array  $filters
     *
     * @return     bool   true, if issue is part of the filter subset
     */
    public function matchesFilter( array $filters ) {

        $result = true;

        foreach ( $filters as $key => $value ) {
            if ( $this->getFieldId( $key ) != $value ) {
                $result = false;
                break;
            }
        }

        return $result;
    }

    /*****************************************************/
    /** MODIFIERS
    /*****************************************************/

    /**
     * Perform some data operations to all issues (see: boards.json: modifiers)
     *
     * Modifiers are used to change the original redmine data and
     * can be applied to any redmine field ("dimension"), see this example:
     *
     * "modifiers": {
     *     "project": [ "stripParentheses" ],
     *     "category": [ "unifyIdsByName" ]
     * }
     *
     * @param      array  $config
     *
     * @return     true
     */
    public function applyDataModifiers( array $config ) {

        foreach ( $config as $dimension => $modifiers ) {
            foreach ( $modifiers as $modifier ) {
                $this->$modifier( $dimension );
            }
        }

        return true;
    }

    /**
     * Remove text within parentheses from name
     *
     * @param      string  $dimension  dimension name (e.g. project)
     *
     * @return     true
     */
    private function stripParentheses( string $dimension ) {

        if ( false !== ( $pos = strpos( $this->data[$dimension]['name'], ' (' ) ) ) {
            $this->data[$dimension]['name'] = substr( $this->data[$dimension]['name'], 0, $pos );
        }

        return true;
    }

    /**
     * Creates new IDs based on the name to unify identical values (e.g. category names)
     *
     * @param      string  $dimension  dimension name (e.g. category)
     *
     * @return     true
     */
    private function unifyIdsByName( string $dimension ) {

        $hash = substr( md5( $this->data[$dimension]['name'] ), 16, 4 );
        $this->data[$dimension]['id'] = hexdec( $hash );

        return true;
    }

    /*****************************************************/
    /** CALC
    /*****************************************************/

    /**
     * Calculate additional fields (according to boards.json: calc)
     *
     * Each field is calculated based on the given calculation method
     * and extra parameters (if needed), see this example:
     *
     * "calc": {
     *     "effort": [ "importFromCustomField", "Restaufwand" ],
     *     "remaining_days": [ "calculateRemainingDays" ],
     *     "is_overdue": [ "calculateOverdueState" ],
     *     "last_updated": [ "calculateLastUpdate" ]
     * }
     *
     * @param      array  $config
     *
     * @return     bool   true
     */
    public function calculateAdditionalFields( array $config ) {

        foreach ( $config as $calc_field => $params ) {
            $method = array_shift( $params );
            $this->$method( $calc_field, $params );
        }

        return true;
    }

    /**
     * Import a calc field from a custom field
     *
     * @param      string  $calc_field
     * @param      array   $config
     *
     * @return     true
     */
    private function importFromCustomField( string $calc_field, array $config ) {

        $custom_field = $config[0];

        $value = null;

        foreach ( $this->data['custom_fields'] as $field ) {

            if ( $field['name'] === $custom_field ) {
                $value = $field['value'];
                break;
            }
        }

        $this->data['_calc'][$calc_field] = $value;

        return true;
    }

    /**
     * Calculates the remaining days
     *
     * @param      string  $calc_field
     *
     * @return     true
     */
    private function calculateRemainingDays( string $calc_field ) {

        $remaining_days = null;

        if ( true === isset( $this->data['due_date'] ) ) {

            // due time: 20 pm UTC ("end of business")
            $due = strtotime( $this->data['due_date'] ) + 20 * 60 * 60;
            $remaining_days = round( ( $due - time() ) / 86400, 1 );
        }

        $this->data['_calc'][$calc_field] = $remaining_days;

        return true;
    }

    /**
     * Calculates if the issue is overdue
     *
     * @param      string  $calc_field
     *
     * @return     true
     */
    private function calculateOverdueState( string $calc_field ) {

        $overdue_state = false;

        if ( true === isset( $this->data['due_date'] ) ) {

            // due time: 20 pm UTC ("end of business")
            $due = strtotime( $this->data['due_date'] ) + 20 * 60 * 60;
            $remaining_days = round( ( $due - time() ) / 86400, 1 );
            $overdue_state = ( $remaining_days < 0 ) ? true : false;
        }

        $this->data['_calc'][$calc_field] = $overdue_state;

        return true;
    }

    /**
     * Calculates days since last update
     *
     * @param      string  $calc_field
     *
     * @return     true
     */
    private function calculateLastUpdate( string $calc_field ) {

        $last_updated = round( ( time() - strtotime( $this->data['updated_on'] ) ) / 86400, 1 );

        $this->data['_calc'][$calc_field] = $last_updated;

        return true;
    }

    /*****************************************************/
    /** STYLE
    /*****************************************************/

    /**
     * Set issue style values (according to boards.json: style)
     *
     * This is used to set the style attributes for the board cards
     * with the given methods and parameters, see this example:
     *
     * "style": {
     *     "color": [ "setStyleByThresholds", "remaining_days", [ -10000, 0, 3, 7, 10000 ] ],
     *     "border": [ "setStyleByIds", "priority", [ "Hoch", "Normal", "Niedrig" ] ],
     *     "size": [ "setStyleByThresholds", "effort", [ 0, 1, 2, 4, 8 ] ],
     *     "custom": {
     *         "state-emergency" : { "priority" : "Notfall" }
     *     }
     * }
     *
     * @param      array  $config
     *
     * @return     true
     */
    public function setStyleInformation( array $config ) {

        foreach ( $config as $style => $params ) {

            if ( $style === "custom" ) {
                $this->setCustomStyles( $params );
            } else {
                $method = array_shift( $params );
                $this->$method( $style, $params );
            }
        }

        return true;
    }

    /**
     * Sets style attribute by threshold values
     *
     * example: "size": [ "setStyleByThresholds", "effort", [ 0, 1, 2, 4, 8 ] ]
     *
     * @param      string  $style
     * @param      array   $config
     *
     * @return     true
     */
    private function setStyleByThresholds( string $style, array $config ) {

        $field = $config[0];
        $steps = $config[1];

        $value = $this->getFieldValue( $field );

        sort( $steps );

        $result = 0;
        foreach ( $steps as $i => $step ) {
            if ( $value > $step ) {
                $result += 1;
            }
        }

        $this->data['_style'][$style] = $result;

        return true;
    }

    /**
     * Sets style attribute by distinct values
     *
     * example: "border": [ "setStyleByIds", "priority", [ "Hoch", "Normal", "Niedrig" ] ]
     *
     * @param      string  $style
     * @param      array   $config
     *
     * @return     true
     */
    private function setStyleByIds( string $style, array $config ) {

        $field = $config[0];
        $ids = $config[1];

        $value = $this->getFieldValue( $field );

        $result = ( false === ( $id = array_search( $value, $ids ) ) ) ? 0 : $id + 1;

        $this->data['_style'][$style] = $result;

        return true;
    }

    /**
     * Sets custom styles by field definition
     *
     * example: "custom": { "state-emergency" : { "priority" : "Notfall" } }
     *
     * @param      array   $config
     *
     * @return     true
     */
    private function setCustomStyles( array $config ) {

        $collect_styles = [];

        // set custom styles if all fields in definition match config values
        foreach ( $config as $custom_style => $definition ) {

            $check_failed = false;

            foreach ( $definition as $field => $config_value ) {
                if ( $config_value !== $this->getFieldValue( $field ) ) {
                    $check_failed = true;
                    break;
                }
            }

            if ( $check_failed === false ) {
                $collect_styles[] = $custom_style;
            }
        }

        $this->data['_style']['custom'] = implode( " ", $collect_styles );

        return true;
    }

}
