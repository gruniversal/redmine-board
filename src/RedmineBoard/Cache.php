<?php

namespace RedmineBoard;

use \Gruniversal\Lib;

/**
 * Simple json-file based cache
 *
 * The data is stored in jsonized arrays that expires after a given time.
 * A garbage collection can be used to remove expired values.
 *
 * Each entry is stored in a separate json based on it's name and an array of params.
 *
 * @see     https://gitlab.com/gruniversal/redmine-board
 * @license https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de
 * @author  David Gruner (mailto:gruniversal@gmx.de / https://www.gruniversal.de)
 *
 */
class Cache {

    // path to store the json files
    private $path;

    // maximum cache time in seconds before cache invalidates (set in constructor)
    private $max_age;

    // cache entry timestamp of last getValue() call
    private $tstamp;

    // maximum cache time in seconds before cache files are deleted (one week)
    private $max_age_garbage = 7 * 86400;

    /*****************************************************/
    /** GET METHODS
    /*****************************************************/

    /**
     * Return cache timestamp from last getValue() call
     *
     * @return     integer  timestamp
     */
    public function getLastTimestamp() {
        return $this->tstamp;
    }

    /*****************************************************/
    /** INITIALIZATION
    /*****************************************************/

    /**
     * Constructor incl. configuration check and garbage collection
     *
     * @param      string              $path     path to store the json files
     * @param      integer             $max_age  maximum cache time in seconds
     *
     * @throws     ConfigurationError
     *
     * @return     RedmineBoardCache
     */
    public function __construct( string $path, int $max_age = 3600 ) {

        // save params
        $this->max_age = $max_age;
        $this->path = $path;

        // check if cache directory exists (create, if not)
        if ( false === is_dir( $this->path ) ) {
            mkdir( $this->path, 0777, true );
            if ( false === is_dir( $this->path ) ) {
                throw new Lib\ConfigurationError( "redmine cache directory does not exist and could not be created" );
            }
        }

        // check if cache directory is writable
        if ( false === is_writable( $this->path ) ) {
            throw new Lib\ConfigurationError( "redmine cache directory is not writeable" );
        }

        // delete expired values
        $this->runGarbageCollection();

        return $this;
    }

    /*****************************************************/
    /** DATA MANAGEMENT
    /*****************************************************/

    /**
     * Returns path and name of cache file
     *
     * @param      string  $value   name
     * @param      array   $params  params
     *
     * @return     string  filename
     */
    public function getFilename( string $value, array $params = [] ) {

        $hash = $value . '_' . md5( json_encode( $params ) );
        $cache_file = $this->path . '/' . $hash . '.json';

        return $cache_file;
    }

    /**
     * Returns the cache entry (if found and not expired)
     *
     * @param      string  $value   name
     * @param      array   $params  params
     *
     * @return     array   cache entry or false, if not found
     */
    public function getValue( string $value, array $params = [] ) {

        $cache_file = $this->getFilename( $value, $params );

        // use cache if non-expired entry exists
        if ( true === is_readable( $cache_file ) ) {
            if ( ( time() - filemtime( $cache_file ) ) < $this->max_age ) {
                $json = file_get_contents( $cache_file );
                $data = json_decode( $json, true );
                $this->tstamp = filemtime( $cache_file );

                return $data;
            }
        }

        return false;
    }

    /**
     * Set a cache entry
     *
     * @param      string  $value   name
     * @param      array   $params  params
     * @param      array   $data
     *
     * @return     true
     */
    public function setValue( string $value, array $params = [], array $data = [] ) {

        $cache_file = $this->getFilename( $value, $params );
        file_put_contents( $cache_file, json_encode( $data ) );

        return true;
    }

    /**
     * Delete a cache entry
     *
     * @param      string  $value   name
     * @param      array   $params  params
     *
     * @return     true
     */
    public function deleteValue( string $value, array $params = [] ) {

        $cache_file = $this->getFilename( $value, $params );
        if ( true === is_writable( $cache_file ) ) {
            unlink( $cache_file );
        }

        return true;
    }

    /**
     * Delete expired cache files (garbage collection)
     *
     * @return     integer  number of deleted files
     */
    public function runGarbageCollection() {

        $deleted = 0;

        foreach ( glob( $this->path . '/*.json' ) as $cache_file ) {

            if ( ( true === is_writable( $cache_file ) ) &&
                ( ( time() - filemtime( $cache_file ) ) > $this->max_age_garbage ) ) {

                unlink( $cache_file );

                $deleted += 1;
            }
        }

        return $deleted;
    }

}
