<?php

namespace RedmineBoard;

use \Gruniversal\Lib;

/**
 * Connects to Redmine and gets necessary data
 *
 * @uses    PHP Redmine API (https://github.com/kbsali/php-redmine-api/)
 *
 * @see     https://gitlab.com/gruniversal/redmine-board
 * @license https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de
 * @author  David Gruner (mailto:gruniversal@gmx.de / https://www.gruniversal.de)
 *
 */
class Connection {

    // configuration --> see setConfig()
    private $config;

    // Redmine API instance
    private $redmine;

    // cache instance
    private $cache;

    // current number of issues
    private $count;

    // maximum number of issues
    private $limit;

    // timestamp of last redmine request
    private $tstamp;

    // used for different cache files
    private const CACHE_PREFIX_BOARD   = 'board';
    private const CACHE_PREFIX_REQUEST = 'request';

    /*****************************************************/
    /** GET METHODS
    /*****************************************************/

    public function getTimestamp() {
        return $this->tstamp;
    }

    public function getCount() {
        return $this->count;
    }

    /*****************************************************/
    /** INITIALIZATION
    /*****************************************************/

    /**
     * Constructor that also loads config
     *
     * @param      array                   $config
     *
     * @return     \RedmineBoard\Connection
     */
    public function __construct( array $config ) {

        $this->setConfig( $config );

        $this->limit = intval( $this->config['connection']['limit'] ) ?? 1000;

        // enable cache (if configured)
        if ( true === $this->config['cache']['active'] ) {
            $this->cache = new Cache(
                $this->config['cache']['path'],
                $this->config['cache']['seconds']
            );
        }

        return $this;
    }

    /**
     * Check and apply configuration
     *
     * @param      array               $config  (see RedmineBoardGlobalConfig:redmine)
     *
     * @throws     ConfigurationError
     *
     * @return     self
     */
    private function setConfig( array $config ) {

        // check given config
        if ( true === empty( $config ) ) {
            throw new Lib\ConfigurationError( "configuration for redmine connection not found" );
        }
        if ( 'https://' !== mb_substr( $config['connection']['server'], 0, 8 ) ) {
            throw new Lib\ConfigurationError( "redmine.server should start with https://" );
        }
        if ( 40 !== mb_strlen( $config['connection']['api-key'] ) ) {
            throw new Lib\ConfigurationError( "redmine.api-key should have 40 characters" );
        }

        $this->config = $config;

        // set default values
        $this->config['connection']['limit'] = $this->config['connection']['limit'] ?? 1000;
        $this->config['cache']['active'] = $this->config['cache']['active'] ?? false;

        return $this;
    }

    /*****************************************************/
    /** DATA MANAGEMENT
    /*****************************************************/

    /**
     * Get list of redmine issues
     *
     * The request uses a parameter array, which return the corresponding issues:
     *
     * tracker_id           only return issues for this tracker(s)
     * project_id           only return issues for this project(s)
     * assigned_to_id       only return issues for this assignee(s)
     *
     * Ids can be left empty to return all issues.
     *
     * Note: also allows a list of ids for params, e.g. assigned_to_id = "1,2,3"
     *
     * See redmine REST API for more information:
     * https://www.redmine.org/projects/redmine/wiki/Rest_Issues
     *
     * @param      array  $params
     */
    public function getBoardIssues( array $params ) {

        Lib\TimeDebug::set( __METHOD__ . " " . json_encode( $params ) );

        $params = $this->sanitizeParams( $params );

        // if cache is enabled and a valid cache entry present --> use it
        if ( $issues = $this->getBoardIssuesFromCache( $params ) ) {
            return $issues;
        }

        // if no cache is present: get data from redmine (and cache it if cache is active)
        $issues = $this->getBoardIssuesFromRedmine( $params );

        Lib\TimeDebug::set( __METHOD__ . " end" );

        return $issues;
    }

    /**
     * Refresh cache for redmine issues
     *
     * @param      array        $params
     *
     * @return     int|bool     number of refreshed issues | false if not possible
     */
    public function refreshBoardIssues( array $params ) {

        Lib\TimeDebug::set( __METHOD__ . " " . json_encode( $params ) );

        $params = $this->sanitizeParams( $params );

        // if cache cannot be loaded --> use getBoardIssues to retrieve all issues
        if ( false === ( $issues = $this->getBoardIssuesFromCache( $params ) ) ) {
            $issues = $this->getBoardIssues( $params );
            $this->saveBoardIssuesToCache( $params, $issues );
            return count( $issues );
        }

        // reduce timestamp by 10 seconds to compensate request time
        $this->tstamp = $this->tstamp - 10;

        $aux = date_default_timezone_get();
        date_default_timezone_set( "UTC" );

        $request_params = $params;
        $request_params['updated_on'] = ">=" . date( "Y-m-d", $this->tstamp ) . "T" . date( "H:i:s", $this->tstamp ) . "Z";

        date_default_timezone_set( $aux );

        // Step 1: update open issues

        $request_params['status_id'] = "open";
        $request_list = $this->createRequestList( $request_params );

        $issues_to_update = [];

        // then run each request and collect issues
        foreach ( $request_list as $request ) {
            $issues_to_update = array_merge( $issues_to_update, $this->requestIssuesFromRedmine( $request ) );
        }

        // refresh issues (update or create)
        foreach ( $issues_to_update as $id => $issue ) {
            $issues[$id] = $issue;
        }

        // Step 2: update closed issues

        $request_params['status_id'] = "closed";
        $request_list = $this->createRequestList( $request_params );

        $issues_to_delete = [];

        // then run each request and collect issues
        foreach ( $request_list as $request ) {
            $issues_to_delete = array_merge( $issues_to_delete, $this->requestIssuesFromRedmine( $request ) );
        }

        // delete issues
        foreach ( $issues_to_delete as $id => $issue ) {
            unset( $issues[$id] );
        }

        // hard cap: maximum number of issues overall
        if ( count( $issues ) > $this->limit ) {
            $issues = array_slice( $issues, 0, $this->limit );
        }

        $this->saveBoardIssuesToCache( $params, $issues );

        Lib\TimeDebug::set( __METHOD__ . " end" );

        return count( $issues_to_update ) + count( $issues_to_delete );
    }

    /**
     * Filter params array to ensure only allowed params are requested
     *
     * @see: https://www.redmine.org/projects/redmine/wiki/Rest_Issues
     *
     * @param      array  $params  The parameters
     */
    private function sanitizeParams( array $params ) {

        $allowed_params = [
            'tracker_id',
            'project_id',
            'assigned_to_id'
        ];

        $params = array_intersect_key( $params, array_flip( $allowed_params ) );

        // maximum number of issues per request = maximum number of issues overall
        $params['limit'] = $this->limit;

        return $params;
    }

    /*****************************************************/
    /** REDMINE
    /*****************************************************/

    /**
     * Connect to redmine server
     *
     * @throws     ConnectionError
     *
     * @return     self
     */
    private function connectToRedmine() {

        Lib\TimeDebug::set( __METHOD__ );

        // only connect once
        if ( true === isset( $this->redmine ) ) {
            return $this;
        }

        $handle = new \Redmine\Client\NativeCurlClient( $this->config['connection']['server'], $this->config['connection']['api-key'] );
        if ( false === $handle->getApi( 'user' )->getCurrentUser() ) {
            throw new Lib\ConnectionError( "redmine login failed" );
        }

        $this->redmine = $handle;

        Lib\TimeDebug::set( __METHOD__ . " end" );

        return $this;
    }

    /**
     * Gets the issues from redmine (and cache it if cache is active)
     *
     * @param      array    $params
     *
     * @return     array    list of issues
     */
    private function getBoardIssuesFromRedmine( array $params ) {

        // reset vars
        $issues = [];
        $this->count = 0;

        // split request into single requests and fill request list (to support list of ids)
        $request_list = $this->createRequestList( $params );

        // then run each request and collect issues
        foreach ( $request_list as $request ) {

            $issues = array_merge( $issues, $this->requestIssues( $request ) );

            // hard cap: maximum number of issues overall
            if ( count( $issues ) > $this->limit ) {
                $issues = array_slice( $issues, 0, $this->limit );
                break; // stop requests if cap is hit
            }
        }

        // if cache is active --> save to cache
        $this->saveBoardIssuesToCache( $params, $issues );

        // save timestamp
        $this->tstamp = time();

        // update global count
        $this->count = count( $issues );

        return $issues;
    }

    /**
     * Create a list of single requests from parameter array
     *
     * Since the Redmine API does not allow requests with multiple IDs, all
     * necessary combinations from the parameter array are broken down into
     * individual calls in a recursive way.
     *
     * @param      array  $params
     * @param      array  $request_part     partially finished list of requests (recursion)
     *
     * @return     array  list of requests
     */
    private function createRequestList( array $params, array $request_part = [] ) {

        $list = [];

        // walk through params if not empty
        if ( false === empty( $params ) ) {

            // associative array_pop()
            $pop = current( $params );
            $key = key( $params );
            unset( $params[$key] );

            // split params by comma
            foreach ( explode( ',', $pop ) as $value ) {

                // recursion: further break down with partially finished list of requests
                $request_part[$key] = trim( $value );

                // end recursion if params is empty (return one-item list)
                if ( 0 === count( $params ) ) {
                    return [$request_part];
                }

                $list = array_merge( $list, $this->createRequestList( $params, $request_part ) );
            }

        }

        return $list;
    }

    /**
     * Request issues (use cache if available)
     *
     * The individual request queries are also cached here in order
     * to be able to use any overlaps between the boards.
     *
     * @param      array     $request
     *
     * @return     array     $issues
     */
    private function requestIssues( array $request ) {

        Lib\TimeDebug::set( __METHOD__ . " " . json_encode( $request ) );

        $issues = [];

        // if cache is enabled and a valid cache entry present --> use it
        if ( ( true === $this->config['cache']['active'] ) &&
            $issues = $this->cache->getValue( self::CACHE_PREFIX_REQUEST, $request ) ) {
        } else {
            $issues = $this->requestIssuesFromRedmine( $request );
        }

        Lib\TimeDebug::set( __METHOD__ . " end" );

        return $issues;
    }

    /**
     * Request issues from Redmine (and cache it if cache is active)
     *
     * The individual request queries are also cached here in order
     * to be able to use any overlaps between the boards.
     *
     * @param      array     $request
     *
     * @throws     ConnectionError
     *
     * @return     array     $issues
     */
    private function requestIssuesFromRedmine( array $request ) {

        Lib\TimeDebug::set( __METHOD__ . " " . json_encode( $request ) );

        $issues = [];

        // connect to redmine and get data
        $this->connectToRedmine();

        // use semaphore to prevent concurrent requests (e.g. from different users)
        Lib\TimeDebug::set( __METHOD__ . " waiting to acquire semaphore" );
        $semaphore = sem_get( ftok( __FILE__, 'r' ) );
        sem_acquire( $semaphore );
        Lib\TimeDebug::set( __METHOD__ . " semaphore acquired in " . Lib\TimeDebug::formatAsSeconds( Lib\TimeDebug::getDiffElapsed() ) . "s" );

        $response = $this->redmine->getApi( 'issue' )->all( $request );

        sem_release( $semaphore );

        if ( array( false ) === $response ) {
            throw new Lib\ConnectionError( "server returned HTTP " . $this->redmine->getLastResponseCode() );
        }

        // add issues to list
        if ( true === isset( $response['issues'] ) ) {
            foreach ( $response['issues'] as $key => $value ) {

                // drop description (not used, saves storage and time)
                $value['description'] = '';

                // add # here to prevent php from renumbering the ids (e.g. in array_merge)
                $issues[ '#' . $value['id'] ] = $value;
            }
        }

        // if cache is active --> save to cache
        if ( true === $this->config['cache']['active'] ) {
            $this->cache->setValue( self::CACHE_PREFIX_REQUEST, $request, $issues );
        }

        Lib\TimeDebug::set( __METHOD__ . " end" );

        return $issues;
    }

    /*****************************************************/
    /** CACHE
    /*****************************************************/

    /**
     * Gets the issues from cache (if active and not expired)
     *
     * @param      array        $params
     *
     * @return     array|bool   issues | false, if issues could not be retrieved
     */
    private function getBoardIssuesFromCache( array $params ) {

        Lib\TimeDebug::set( __METHOD__ . " " . json_encode( $params ) );

        if ( true !== $this->config['cache']['active'] ) {
            return false;
        }

        if ( $result = $this->cache->getValue( self::CACHE_PREFIX_BOARD, $params ) ) {

            $issues = $result;
            $this->tstamp = $this->cache->getLastTimestamp();
            $this->count = count( $issues );

            return $issues;
        }

        return false;
    }

    /**
     * Saves the issues to cache (if active)
     *
     * @param      array  $params
     * @param      array  $issues
     *
     * @return     bool   false, if issues could not be written to cache
     */
    private function saveBoardIssuesToCache( array $params, array $issues ) {

        Lib\TimeDebug::set( __METHOD__ . " " . json_encode( $params ) );

        if ( true !== $this->config['cache']['active'] ) {
            return false;
        }

        return $this->cache->setValue( self::CACHE_PREFIX_BOARD, $params, $issues );
    }
}
