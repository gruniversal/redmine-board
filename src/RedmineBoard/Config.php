<?php

namespace RedmineBoard;

use \Gruniversal\Lib;

/**
 * Holds configuration for the current board
 *
 * @see     https://gitlab.com/gruniversal/redmine-board
 * @license https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de
 * @author  David Gruner (mailto:gruniversal@gmx.de / https://www.gruniversal.de)
 *
 */
class Config {

    // board configuration (see: boards.json)
    private $config = [];

    // list of calculated fields
    private $calc_fields = [];

    /*****************************************************/
    /** GET METHODS
    /*****************************************************/

    public function getConfig() {
        return $this->config;
    }

    public function getName() {
        return $this->config['name'];
    }

    public function getTrackers() {
        return $this->config['trackers'];
    }

    public function getProjects() {
        return $this->config['projects'];
    }

    public function getAssignees() {
        return $this->config['assignees'];
    }

    public function getFilters() {
        return $this->config['filters'];
    }

    public function getColumnsField() {
        return $this->config['columns']['field'];
    }

    public function getColumnsSort() {
        return $this->config['columns']['sort'] ?? false;
    }

    public function getColumnsValues() {
        return $this->config['columns']['values'] ?? [];
    }

    public function getRowsField() {
        return $this->config['rows']['field'];
    }

    public function getRowsSort() {
        return $this->config['rows']['sort'];
    }

    public function getRowsValues() {
        return $this->config['rows']['values'];
    }

    public function getModifiers() {
        return $this->config['modifiers'];
    }

    public function getCalculations() {
        return $this->config['calc'];
    }

    public function getStyles() {
        return $this->config['styles'];
    }

    public function getTemplates() {
        return $this->config['templates'];
    }

    /*****************************************************/
    /** CONSTRUCTOR
    /*****************************************************/

    public function __construct( array $board_config ) {

        $this->config = $board_config;

        // check configuration and add default values
        $this->setupName();
        $this->setupParams();
        $this->setupColumns();
        $this->setupRows();
        $this->setupFilters();
        $this->setupModifiers();
        $this->setupCalculations();
        $this->setupStyles();
        $this->setupTemplates();

        return $this;
    }

    /*****************************************************/
    /** NAME
    /*****************************************************/

    private function setupName() {

        // set default
        if ( false === isset( $this->config['name'] ) ) {
            $this->config['name'] = "(unnamed board)";
        }

        return $this;
    }

    /*****************************************************/
    /** PARAMS
    /*****************************************************/

    private function setupParams() {

        // TODO: add more parameters ?

        // TODO: combine them in $this->config['params'] ?

        // set default: empty values
        if ( false === isset( $this->config['trackers'] ) ) {
            $this->config['trackers'] = "";
        }
        if ( false === isset( $this->config['projects'] ) ) {
            $this->config['projects'] = "";
        }
        if ( false === isset( $this->config['assignees'] ) ) {
            $this->config['assignees'] = "";
        }

        return $this;
    }

    /*****************************************************/
    /** FILTERS
    /*****************************************************/

    private function setupFilters() {

        $defaults = [
            "assigned_to",
            "project",
            "category",
            "author",
            "priority",
            "status",
            "tracker"
        ];

        // "interesting" way to efficiently remove used dimensions from default
        $defaults = array_flip( $defaults );
        unset( $defaults[$this->getColumnsField()] );
        unset( $defaults[$this->getRowsField()] );
        $defaults = array_flip( $defaults );

        // set default: all filters, but not used dimensions of columns and rows
        if ( false === isset( $this->config['filters'] ) ) {
            $this->config['filters'] = $defaults;
        }

        // check structure and verify configuration
        if ( false === is_array( $this->config['filters'] ) ) {
            throw new Lib\ConfigurationError( "filters in board '" . $this->getName() . "' needs to be an array" );
        } else {
            foreach ( $this->config['filters'] as $filter ) {
                if ( false === in_array( $filter, Issue::getRedmineDimensions() ) ) {
                    throw new Lib\ConfigurationError( "filter '" . $filter . "' in board '" . $this->getName() . "' is not recognized" );
                }
            }
        }

        return $this;
    }

    /*****************************************************/
    /** COLUMNS
    /*****************************************************/

    private function setupColumns() {

        // set defaults: tracker type (unsorted)
        if ( false === isset( $this->config['columns'] ) ) {
            $this->config['columns'] = [
                "field" => "tracker",
                "sort" => false,
                "values" => []
            ];
        }

        // check structure and verify configuration
        if ( false === is_array( $this->config['columns'] ) ) {
            throw new Lib\ConfigurationError( "columns in board '" . $this->getName() . "' needs to be an array" );
        } else {

            // fill up with defaults: tracker type (unsorted)
            if ( false === isset( $this->config['columns']['field'] ) ) {
                $this->config['columns']['field'] = "tracker";
            }
            if ( false === isset( $this->config['columns']['sort'] ) ) {
                $this->config['columns']['sort'] = false;
            }
            if ( false === isset( $this->config['columns']['values'] ) ) {
                $this->config['columns']['values'] = [];
            }

            if ( false === isset( $this->config['columns']['field'] ) ) {
                throw new Lib\ConfigurationError( "columns field in board '" . $this->getName() . "' is not set" );
            }
            if ( false === is_array( $this->config['columns']['values'] ) ) {
                throw new Lib\ConfigurationError( "columns values in board '" . $this->getName() . "' needs to be an array" );
            }
        }

        return $this;
    }

    /*****************************************************/
    /** ROWS
    /*****************************************************/

    private function setupRows() {

        // set default: projects (sorted)
        if ( false === isset( $this->config['rows'] ) ) {
            $this->config['rows'] = [
                "field" => "project",
                "sort" => true,
                "values" => []
            ];
        }

        // check structure and verify configuration
        if ( false === is_array( $this->config['rows'] ) ) {
            throw new Lib\ConfigurationError( "rows in board '" . $this->getName() . "' needs to be an array" );
        } else {

            // fill up with defaults: projects (sorted)
            if ( false === isset( $this->config['rows']['field'] ) ) {
                $this->config['rows']['field'] = "project";
            }
            if ( false === isset( $this->config['rows']['sort'] ) ) {
                $this->config['rows']['sort'] = true;
            }
            if ( false === isset( $this->config['rows']['values'] ) ) {
                $this->config['rows']['values'] = [];
            }

            if ( false === isset( $this->config['rows']['field'] ) ) {
                throw new Lib\ConfigurationError( "rows field in board '" . $this->getName() . "' is not set" );
            }
            if ( false === is_array( $this->config['rows']['values'] ) ) {
                throw new Lib\ConfigurationError( "rows values in board '" . $this->getName() . "' needs to be an array" );
            }
        }

        return $this;
    }

    /*****************************************************/
    /** MODIFIERS
    /*****************************************************/

    private function setupModifiers() {

        // set default: no modifications
        if ( false === isset( $this->config['modifiers'] ) ) {
            $this->config['modifiers'] = [];
        }

        // check structure and verify configuration
        if ( false === is_array( $this->config['modifiers'] ) ) {
            throw new Lib\ConfigurationError( "modifiers in board '" . $this->getName() . "' needs to be an array" );
        } else {
            foreach ( $this->config['modifiers'] as $dimension => $modifiers ) {
                if ( false === in_array( $dimension, Issue::getRedmineDimensions() ) ) {
                    throw new Lib\ConfigurationError( "dimension '" . $dimension . "' for modifiers in board '" . $this->getName() . "' is not recognized" );
                }
                if ( false === is_array( $modifiers ) ) {
                    throw new Lib\ConfigurationError( "modifiers for dimension '" . $dimension . "' in board '" . $this->getName() . "' needs to be an array" );
                } else {
                    foreach ( $modifiers as $modifier ) {
                        if ( false === method_exists( '\RedmineBoard\Issue', $modifier ) ) {
                            throw new Lib\ConfigurationError( "modifier '$dimension.$modifier' in board '" . $this->getName() . "' is not recognized" );
                        }
                    }
                }
            }
        }

        return $this;
    }

    /*****************************************************/
    /** CALCULATIONS
    /*****************************************************/

    private function setupCalculations() {

        $defaults = [
            "remaining_days" => [ "calculateRemainingDays" ],
            "is_overdue" => [ "calculateOverdueState" ],
            "last_updated" => [ "calculateLastUpdate" ]
        ];

        // set defaults and overwrite them with config
        if ( false === isset( $this->config['calc'] ) ) {
            $this->config['calc'] = $defaults;
        } else {
            $this->config['calc'] = array_replace_recursive( $defaults, $this->config['calc'] );
        }

        // check structure and verify configuration
        if ( false === is_array( $this->config['calc'] ) ) {
            throw new Lib\ConfigurationError( "calc in board '" . $this->getName() . "' needs to be an array" );
        } else {
            foreach ( $this->config['calc'] as $calc_field => $params ) {
                if ( false === is_array( $params ) ) {
                    throw new Lib\ConfigurationError( "parameters for field '" . $calc_field . "' in board '" . $this->getName() . "' needs to be an array" );
                }
                if ( false === method_exists( 'RedmineBoard\Issue', $params[0] ) ) {
                    throw new Lib\ConfigurationError( "calculation '$params[0]' for field '" . $calc_field . "' in board '" . $this->getName() . "' is not recognized" );
                }
            }
        }

        // save calculated fields
        $this->calc_fields = array_keys( $this->config['calc'] );

        return $this;
    }

    /*****************************************************/
    /** STYLES
    /*****************************************************/

    private function setupStyles() {

        $defaults = [
            "color" => [ "setStyleByThresholds", "remaining_days", [ -10000, -30, 0, 3, 7, 10000 ] ]
        ];

        // set defaults and overwrite them with config
        if ( false === isset( $this->config['styles'] ) ) {
            $this->config['styles'] = $defaults;
        } else {
            $this->config['styles'] = array_replace_recursive( $defaults, $this->config['styles'] );
        }

        // check structure and verify configuration
        if ( false === is_array( $this->config['styles'] ) ) {
            throw new Lib\ConfigurationError( "styles in board '" . $this->getName() . "' need to be an array" );
        }
        foreach ( $this->config['styles'] as $style_field => $params ) {
            if ( false === in_array( $style_field, [ 'color', 'border', 'size', 'custom' ] ) ) {
                throw new Lib\ConfigurationError( "style '" . $style_field . "' in board '" . $this->getName() . "' is not known" );
            }
            if ( $style_field === "custom" ) {
                if ( false === is_array( $params ) ) {
                    throw new Lib\ConfigurationError( "custom styles in board '" . $this->getName() . "' need to be an array" );
                } else {
                    foreach ( $params as $custom_style => $definition ) {
                        if ( false === is_array( $definition ) ) {
                            throw new Lib\ConfigurationError( "definition for custom style '" . $custom_style . "' in board '" . $this->getName() . "' needs to be an array" );
                        }
                    }
                }
            } else {
                if ( false === method_exists( '\RedmineBoard\Issue', $params[0] ) ) {
                    throw new Lib\ConfigurationError( "calculation '$params[0]' for style '" . $style_field . "' in board '" . $this->getName() . "' is not recognized" );
                }
            }

        }

        return $this;
    }

    /*****************************************************/
    /** TEMPLATES
    /*****************************************************/

    private function setupTemplates() {

        $known_templates = [
            'header',
            'headline',
            'filter',
            'board',
            'subline',
            'custom',
            'footer'
        ];

        // set default: use templates in folder /twig/
        if ( false === isset( $this->config['templates'] ) ) {
            $this->config['templates'] = [];
        }

        // check structure and verify configuration
        if ( false === is_array( $this->config['templates'] ) ) {
            throw new Lib\ConfigurationError( "templates in board '" . $this->getName() . "' needs to be an array" );
        }
        foreach ( $this->config['templates'] as $template => $location ) {
            if ( false === in_array( $template, $known_templates ) ) {
                throw new Lib\ConfigurationError( "template '" . $template . "' in board '" . $this->getName() . "' is not known" );
            }
        }

        return $this;
    }

}
