<?php

/**
 * Global configuration file
 *
 * This configuration is defined in PHP code to avoid your configuration from being read from web.
 *
 */

class RedmineBoardGlobalConfig {

    // set your configuration here to override the defaults (see below)
    private static $config = [

        "redmine" => [

            "connection" => [
                // set up your server and api-key here
                "server" => "",
                "api-key" => ""
            ],

            "cache" => [
                // enable cache to increase speed
                "active" => true,
                "path" => "/tmp/redmineboard/redmine/"
            ]

        ],

        "twig" => [

            "cache" => [
                // enable cache to increase speed
                "active" => true,
                "path" => "/tmp/redmineboard/twig/"
            ]
        ]
    ];

    // --- do not alter anything below this line ---

    // default values and documentation
    private static $defaults = [

        // this enables a simple debug box
        "debug" =>  false,

        // default theme: /themes/vanilla
        "theme" => "vanilla",

        // board configuration
        "boards" => [
            // location of boards config
            "config" => REDMINEBOARD_CONFIG_DIR . '/boards.json',
            // this board will be loaded if no board-id is given
            "default" => 1
        ],

        // redmine configuration
        "redmine" => [

            "connection" => [
                // url to redmine location: e.g. https://redmine.org/
                "server" => "",
                // redmine api key, see https://www.redmine.org/projects/redmine/wiki/rest_api#Authentication
                "api-key" => "",
                // maximum of tickets
                "limit" => "1000"
            ],

            "cache" => [
                // enable cache to increase speed
                "active" => false,
                "path" => "/tmp/redmineboard/redmine/",
                // cache renews after (default: 60 minutes)
                "seconds" => "3600"
            ]

        ],

        // template engine configuration
        "twig" => [

            "cache" => [
                // enable cache to increase speed
                "active" => false,
                "path" => "/tmp/redmineboard/twig/"
            ]
        ]
    ];

    /**
     * Return configuration based on defaults and own settings
     *
     * @return     array
     */
    public static function getConfig() {

        $config = array_replace_recursive( self::$defaults, self::$config );

        return $config;
    }
}
