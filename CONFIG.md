# Redmine Board - Konfiguration

Die Konfiguration ist in zwei Teile aufgeteilt:

```

\config\
|
+- RedmineBoardGlobalConfig.php		- globale Konfiguration
|
+- boards.json						- Konfiguration der Boards

```

Das Verzeichnis ist durch eine .htaccess sowie eine index.php vor neugierigen Blicken geschützt, kann aber auch in einen Pfad gelegt werden, der nicht über den Webserver erreichbar ist.

In diesem Fall ist die Konstante `REDMINEBOARD_CONFIG_DIR` in der `index.php`anzupassen.

## Globale Konfiguration

Diese Konfiguration ist grundlegend und wirkt sich auf alle Boards aus.

Hier werden folgende Einstellungen vorgenommen:
* Redmine Verbindung
* Redmine Cache
* Twig Cache
* Standard Board
* Ordner für Board-Konfiguration
* genutztes Theme
* Debug Modus

Eine Dokumentation der Parameter findet sich in [RedmineBoardGlobalConfig.php](./config/RedmineBoardGlobalConfig.php) in der Variablen `$defaults`.

## Board-Konfiguration

In der Board-Konfiguration werden die verschiedenen Boards als Liste in einer JSON konfiguriert.

Der Ablageort der JSON kann in der globalen Konfiguration hinterlegt werden (Standard: [boards.json](./config/boards.json)).

Eine weitergehende Dokumentation der einzelnen Parameter findet sich in [RedmineBoardConfig](./src/RedmineBoardConfig.php).

### Minimale Konfiguration

Die minimale Konfiguration besteht aus einer eindeutigen Board-ID sowie einem Namen:

```json
{
	"1": {
		"name":				"Mein Redmine Board"
	}
}
```

Alle weiteren Parameter sind optional und beeinflussen die angezeigten Daten sowie deren Darstellung.

### Abfrageparameter

Hiermit wird festgelegt, welche Trackertypen, Projekte und Bearbeiter in die Abfrage einbezogen werden:

```json
		"trackers":			"zu berücksichtigende Tracker-IDs (oder leer)",
		"projects":			"zu berücksichtigende Projekt-IDs (oder leer)",
		"assignees":		"zu berücksichtigende Benutzer-IDs (oder leer)",
```

Als Angabe ist jeweils ein leerer String, eine Zahl oder eine Kommaliste (z.B. "28,32") möglich.

Die Zahlen beziehen sich auf die IDs der jeweiligen Felder im Redmine.

Standard-Konfiguration: Ohne Angabe werden alle Tickets dargestellt.

### Zeilen und Spalten

Dies definiert die Darstellung der Matrix nach Spalten und Zeilen.

```json
		"columns": {
			"field":		"status",
			"sort":			false,
			"values": [
				"Backlog",
				"ToDo",
				"In Progress",
				"Internal Approval",
				"Client Approval",
				"Release"
			]
		},

		"rows": {
			"field":		"assigned_to",
			"sort":			true
		}
```

Über `field` wird ausgewählt welche Dimension in der Zeile bzw. Spalte angezeigt wird.

Aktuell sind folgende Dimensionen unterstützt:
* `project`
* `tracker`
* `status`
* `priority`
* `author`
* `assigned_to`
* `category`

Über `values` können vorgegebene Werte angegeben werden (z.B. um eine bestimmte Reihenfolge vorzugeben). Kommen im Import Werte vor, die damit nicht übereinstimmen, werden diese automatisch in der Reihenfolge, mit der sie eingelesen werden ergänzt.

Wenn `sort` auf true gesetzt wird, werden die Zeilen bzw. Spalten automatisch sortiert.

Standard-Konfiguration: Ohne Angabe werden die Tickets nach Projekten (Zeilen) und Trackertypen (Spalten) dargestellt.

### Filter

Dies definiert, welche Filteroptionen im Board angezeigt werden.

Hier eine Konfiguration, die alle verfügbaren Filter aktiviert:

```json
		"filters": [
			"assigned_to",
			"project",
			"category",
			"author",
			"priority",
			"status",
			"tracker"
		],
```

In der Regel ist es sinnvoll die als Zeilen und Spalten gewählten Dimensionen nicht zusätzlich als Filter anzuzeigen.

Standard-Konfiguration: Ohne Angabe werden alle verfügbaren Filter dargestellt, sofern die Dimension nicht in den Zeilen oder Spalten genutzt werden.

### Import-Modifizierer

Hiermit können auf einzelne Felder beim Import bestimmte Modifizierer angewendet werden.

```json
		"modifiers": {
			"project": [ "stripParentheses" ],
			"category": [ "unifyIdsByName" ]
		},
```

Verfügbare Modifizierer:

`stripParentheses`
* entfernt Klammerausdrücke im importierten Feld (z.B. bei Projektnamen)

`unifyIdsByName`
* dies verwirft die von Redmine importierte ID eines Feldes und berechnet diese anhand des Namens neu
* das ist nützlich um gleichnamige Feldeinträge zu einer ID zusammenzufassen (z.B. bei Kategorien)

Standard-Konfiguration: Ohne Angabe werden keine Modifizierer angewendet.

Eine weitergehende Dokumentation der einzelnen Modifizierer findet sich in [RedmineBoardIssue](./src/RedmineBoardIssue.php).

### Berechnungen

Berechnungen werden genutzt, um importierte Daten aus Redmine über zusätzliche Berechnungen auszuwerten.

Die Konfiguration ist dabei eine Liste der Namen der berechneten Felder sowie deren Berechnungsmethodik.

Anders als Dimensionen können berechnete Felder nicht für Zeilen/Spalten oder Filter genutzt werden.

Dafür werden sie aber automatisch zur Berechnung von Anzahlen und Summen genutzt und stehen für Style-Anpassungen zur Verfügung.

```json
		"calc": {
			"effort": [ "importFromCustomField", "Restaufwand" ],
			"remaining_days": [ "calculateRemainingDays" ],
			"is_overdue": [ "calculateOverdueState" ],
			"last_updated": [ "calculateLastUpdate" ]
		},
```

Verfügbare Berechnungen:

`importFromCustomField`
* dies wird genutzt um ein CustomField in ein Berechnungsfeld zu übernehmen
* das ist beispielsweise erforderlich um diese summieren oder für die Style-Anpassung nutzen zu können

`calculateRemainingDays`
* berechnet die Anzahl der Resttage bei gesetztem Fälligkeitsdatum

`calculateOverdueState`
* berechnet, ob das Ticket überfällig ist

`calculateLastUpdate`
* berechnet die Anzahl der Tage seit der letzten Aktualisierung des Tickets

Standard-Konfiguration: Ohne Angabe werden diese Berechnungen durchgeführt:

```json
		"calc": {
			"remaining_days": [ "calculateRemainingDays" ],
			"is_overdue": [ "calculateOverdueState" ],
			"last_updated": [ "calculateLastUpdate" ]
		},
```

Eine weitergehende Dokumentation der einzelnen Berechnungen findet sich in [RedmineBoardIssue](./src/RedmineBoardIssue.php).

### Style-Anpassungen

Über diese Konfiguration können Farbe (`color`), Rahmen (`border`) und Größe (`size`) der Tickets im Board beeinflusst werden.

Die Werte werden als Ganzzahlen berechnet und bei der Darstellung der Tickets im Board über die entsprechende CSS genutzt.

Bei die Berechnung können hierfür sowohl importierte Felder als auch berechnete Felder genutzt werden.

```json
		"styles": {
			"color": [ "setStyleByThresholds", "remaining_days", [ -10000, -30, 0, 3, 7, 10000 ] ],
			"border": [ "setStyleByIds", "priority", [ "Hoch", "Normal", "Niedrig" ] ],
			"size": [ "setStyleByThresholds", "effort", [ 0, 1, 2, 4, 8 ] ]
		},
```

Tickets mit Priorität "Normal" würden in diesem Beispiel mit border = 2 berechnet, was `div.b2` in der CSS entspricht.

Verfügbare Berechnungen:

`setStyleByThresholds`
* setzt Werte anhand von Schwellenwerten

`setStyleByIds`
* setzt Werte anhand von konkreten IDs

Standard-Konfiguration: Ohne Angabe werden diese Style-Angaben berechnet:

```json
		"styles": {
			"color": [ "setStyleByThresholds", "remaining_days", [ -10000, -30, 0, 3, 7, 10000 ] ]
		},
```

Eine weitergehende Dokumentation der einzelnen Berechnungen findet sich in [RedmineBoardIssue](./src/RedmineBoardIssue.php).

### Custom Styles

Zusätzlich können individuelle Stile anhand einer Definition von Feldwerten festgelegt werden.

Hierbei wird beispielsweise die Klasse `div.state-emergency` auf alle Tickets angewendet, deren Feld `priority` den Wert `"Notfall"` haben:

```json
		"styles": {
			"custom": {
				"state-emergency" : { "priority" : "Notfall" }
			}
		},
```

Es wird dabei mit dem exakten Feldwert verglichen, d.h. Bedingungen wie ungleich oder größer/kleiner können nicht modelliert werden.

Sind mehrere Felder angegeben, müssen alle Bedingungen erfüllt sein (Und-Verknüpfung).

Als Definition können sowohl importierte als auch berechnete Felder genutzt werden.

Hier ein komplexeres Beispiel inkl. der normalen Style-Anpassungen:

```json
		"styles": {
			"color": [ "setStyleByThresholds", "remaining_days", [ -10000, -30, 0, 3, 7, 10000 ] ],
			"border": [ "setStyleByIds", "priority", [ "Hoch", "Normal", "Niedrig" ] ],
			"size": [ "setStyleByThresholds", "effort", [ 0, 1, 2, 4, 8 ] ],
			"custom": {
				"state-emergency" : { "priority" : "Notfall" },
				"state-overdue" : { "is_overdue" : true },
				"state-unassigned" : { "assigned_to" : "", "status" : "ToDo" }
			}
		}
```

Zusätzlich zur Definition müssen die genannten Style-Klassen in einer eigenen CSS hinzugefügt werden (siehe auch [THEMES.md](./THEMES.md)).

### Template-Überschreibungen

Dies wird genutzt, um für verschiedene Boards eigene Templates einbinden zu können (falls erforderlich).

Es wird dabei das jeweils genannte Template durch das konfigurierte twig ausgetauscht.

Dieses muss sich im Theme-Ordner befinden. (siehe globale Konfiguration)

```json
		"templates" : {
			"custom": "my_own_template.twig"
		}
```

Standard-Konfiguration: Ohne Angabe werden die Standard-Templates im Theme-Ordner genutzt.

In der Regel sollten Anpassungen in den Standard-Templates im Theme-Ordner erfolgen und nur bei Abweichungen zwischen einzelnen Boards diese über diese Mechanik überschrieben werden.
