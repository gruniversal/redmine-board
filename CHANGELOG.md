# Releases / Changelog

Hier finden sich alle veröffentlichten Versionen.

### Release v1.6.1 (17.02.2024)

**Geänderte Funktionen**

- dynamische Highlight-Suche: Groß-/Kleinschreibung ignorieren

### Release v1.6 (08.02.2024)

**Neue Funktionen**

- dynamische Highlight-Suche zur Hervorhebung von Tickets direkt im Frontend
- Custom Styles zur individuellen Hervorhebung von Tickets ergänzt (siehe [CONFIG.md](CONFIG.md)).

**Geänderte Funktionen**

- Theme "pluspol": Custom Style für Notfall-Tickets
- Theme "pluspol": Tippy lokal einbinden für besseren Datenschutz

**Behobene Fehler**

- dynamische Highlight-Suche: Highlighting nur für das Board / Legende nicht mit ausgrauen
- dynamische Highlight-Suche: initiale Suche, nur wenn es ein Suchwort gibt
- kleinere Code-Optimierungen

### Release v1.5 (05.11.2023)

**Neue Funktionen**

- Issues können nun auch per Strg-Klick im Hintergrund-Tab geöffnet werden
- Exceptions auch im Produktivsystem anzeigen

**Geänderte Funktionen**

- Redmine API auf v2.2.0 angehoben
- Anpassung der veralteten Aufrufe auf `Redmine\Client\NativeCurlClient`
- Twig auf v3.7.1 angehoben

**Optimierungen**

- `phpcs.xml` für automatisches Codestyling hinzugefügt
- Namespaces RedmineBoard und Lib ergänzt
- Autoloading für eigene Klassen integriert
- Polyfill für PHP 8 Stringfunktionen
- Font Barlow lokal eingebunden für besseren Datenschutz
- Caching für Fonts in `.htaccess` aktiviert

**Behobene Fehler**

- fehlerhafte Positionierung von Boxen beim Hovern behoben

**Entfernte Funktionen**

- Support für PHP 7.3

### Release v1.4 (24.09.2023)

**Neue Funktionen**

- Support für PHP 8

**Geänderte Funktionen**

- Redmine API auf v1.6.0 angehoben

### Release v1.3 (03.04.2023)

**Geänderte Funktionen**

- Silent Refresh aktualisiert nun nur noch Tickets ab dem letzten Zeitstempel des Caches
- geschlossene Tickets werden dabei aus dem Board-Cache entfernt
- zudem wird das globale Ticket-Limit beachtet

**Behobene Fehler**

- Silent Refresh funktioniert nun auch mit Multi-Request-Boards
- neu eröffnete Tickets werden ebenfalls aktualisiert

### Release v1.2 (02.04.2023)

**Neue Funktionen**

- sechste Farbe für lange überfällige Tickets (über 30 Tage) ergänzt

**Geänderte Funktionen**

- Refresh-Button färbt sich grün, wenn neue Daten vorliegen
- Spin-Animation während Refresh oder Silent Refresh läuft

### Release v1.1 (01.04.2023)

**Geänderte Funktionen**

- Silent Refresh aktualisiert nun nur noch Daten seit dem letzten Cache-Zeitpunkt
- ist kein Cache vorhanden, werden alle Daten geholt und zwischengespeichert
- Refresh-Button nutzt nun Silent-Refresh-Logik (dadurch tw. deutlich schneller)
- Standard-Cache-Dauer auf 60 Minuten erhöht

**Optimierungen**

- Semaphore implementiert, um parallele Requests zu unterbinden
- Redmine Subrequests werden nun abgebrochen, wenn globales Limit erreicht
- Code-Refactoring: globale Variablen zurückgebaut
- Request Cache speichert nun ebenfalls ohne Ticket-Description

### Release v1.0 (18.02.2023)

**Geänderte Funktionen**

- Standard-Konfiguration für Filter eingefügt (siehe [CONFIG.md](CONFIG.md)).
- Silent Refresh aktualisiert den Cache nur, wenn dieser veraltet ist (also `global.redmine.cache.seconds` verstrichen sind)

**Performance-Optimierungen**

- beim Silent Refresh werden nun die Matrix-Daten nicht mehr aufgearbeitet

**Behobene Fehler**

- Silent Refresh kann nun nur noch genutzt werden, wenn der Cache auch aktiv ist

### Beta 1 (08.08.2022)

**Neue Funktionen**

- Anzahlen und Summen auch für Zeilen und Spalten berechnen
	- damit ist es z.B. möglich die Anzahl der überfälligen Tickets oder der Restaufwände summiert pro Projekt anzuzeigen

**Geänderte Funktionen**

- Standard-Konfiguration für Berechnungen und Styles eingefügt (siehe [CONFIG.md](CONFIG.md)).
- Konfiguration für Anzahl und Summen entfernt (es werden nun automatisch alle Berechnungsfelder genutzt)

**Performance-Optimierungen**

- Berechnung für Anzahlen und Summen verbessert
- Caching bei Boards mit mehreren IDs verbessert

**Behobene Fehler**

- Auswahl "(nicht zugewiesen)" wird nun auch übernommen (siehe Issue #1)
- Theme "pluspol": Position linker Pfeil korrigiert
- Board wird nur noch angezeigt, wenn Daten vorhanden sind

### Alpha 8 (05.08.2022)

**Neue Funktionen**

- autom. Hintergrundaktualisierung der Daten
    - über einen unsichtbaren Frontend-Request (einmal pro Minute)
    - Hintergrund: teilweise dauert die Ermittlung der Daten über 10 Sekunden

**Geänderte Funktionen**

- Warnung bei Begrenzung des Boards durch Konfiguration nun in rot hervorgehoben

**Performance-Optimierungen**

- Verbindung zu Redmine wird nur hergestellt, wenn auch erforderlich
- interne Zeitmessung zur Optimierung der Ladezeit integriert
- festes Zeitlimit für Garbage Collection des Redmine Caches (eine Woche)

## Alpha 7 (24.07.2022)

**Neue Funktionen**

- Dimensionen der Zeilen und Spalten im Board sind nun frei konfigurierbar
	- verfügbare Felder: Bearbeiter, Projekt, Kategorie, Ersteller, Status, Priorität und Typ
	- automatische Sortierung der Zeilen und Spalten (konfigurierbar)
- zusätzliche Filteroptionen: Ersteller, Status, Priorität, Typ
- Filterung nach Tickets ohne Bearbeiter
- Support für eigene Themes sowie individuelle Templates über Board-Konfiguration
	- ermöglicht u.a. Kundenwunsch: Nutzung einer JS-Library statt HTML-Tooltip
- Möglichkeit für übergreifende Boards
	- Angabe mehrerer Trackertypen, Projekte oder Bearbeiter gleichzeitig pro Board möglich

**Geänderte Funktionen**

- umfassendes Refactoring der gesamten Anwendung
    - Datenlogik in neue Klassen aufgeteilt (`RedmineBoardMatrix` und `RedmineBoardIssue`)
    - Konfigurationslogik in Klasse `RedmineBoardConfig` ausgelagert
	- wesentlich flexiblere Konfiguration der Anwendung möglich (u.a. Styling)
	- verbesserte Konfigurationsfehlermeldungen und Standardwerte
    - allgemeine Verbesserungen im Code Style und Umstellung auf englische Kommentare
- Konfiguration in Ordner `/config` verschoben und Zugriff per Web verhindert
	- globale Konfiguration in Klasse `RedmineBoardGlobalConfig` ausgelagert
	- `.htaccess` und `index.php` ergänzt um Zugriff zusätzlich abzusichern
	- eine Dokumentation der Konfiguration findet sich [CONFIG.md](CONFIG.md).

ACHTUNG: Umfangreiche Anpassung der Konfiguration erforderlich.

## Alpha 6 (25.01.2020)

**Neue Funktionen**

- Filterung nach Kategorien (Arbeitsstand, noch nicht fertig!)
	- offen: Auswahl "keine" funktioniert nicht
	- offen: automatische Sortierung der Kategorien sinnvoll (da diese gesammelt werden)
	- offen: automatisches Ausblenden leerer Zeilen sinnvoll
- Anzahl der abgeholten Tickets über globale Konfiguration limitierbar (`global.redmine.limit`)
- Debug-Modus für Entwicklung über Konfiguration aktivierbar (`global.debug`)

**Geänderte Funktionen**

- Cache Handling für Redmine-Daten in separate Klasse ausgelagert
	- Konfiguration angepasst (`global.redmine.cache` statt `global.cache-redmine`)
	- Cache-Verzeichnis wird automatisch angelegt (wenn möglich)
	- automatische Garbage Collection für veraltete Cache-Einträge
- Redmine Verbindung in separate Klasse ausgelagert
	- Prüfung der Redmine Konfiguration verbessert (Defaultwerte)
	- grundsätzliche Unterstützung für mehrere Trackertypen/Projekte bei der Datenabfrage
	- dafür Konfiguration angepasst (`board.trackers` statt `board.tracker-id`, neu: `board.projects` und `board.assignees`)
	- Anpassung der Berechnung der Limit-Beschränkung für mehrfache Anfragen

ACHTUNG: Umfangreiche Anpassung der Konfiguration erforderlich (siehe [example-config.json](example-config.json).)

**Behobene Fehler**

- Page-Title wird nun aus Board-Konfiguration übernommen

**Entfernte Funktionen**

- keine

## Alpha 5 (10.12.2019)

**Neue Funktionen**

- Anzahl der überfälligen Tickets anzeigen
- Gesamtsumme des Restaufwands anzeigen
- Konfiguration mehrerer Boards möglich
	- aktuell nur über URL-Parameter `board` erreichbar
	- Anzeige des Standard-Boards, wenn kein Board gewählt
	- jeweils eine Tracker-ID pro Board konfigurierbar
	- Board-Name in Headline wird aus Konfiguration übernommen

**Geänderte Funktionen**

- Implementierung der Zähler grundsätzlich verbessert (`_countIssue()`)
- Caching der Twig Templates über globale Konfiguration möglich
- Konfiguration des Cachings der Redmine-Daten verändert (nun Pfad- statt Dateiangabe)
- ACHTUNG: Umfangreiche Anpassung der Konfiguration erforderlich (siehe [example-config.json](example-config.json).)

## Alpha 4 (30.11.2019)

**Geänderte Funktionen**

- Verwendung von Composer zur Verwaltung externer Bibliotheken (Redmine API und Template Engine Twig)
- HTML-Ausgabe vollständig auf Twig umgestellt (außer Hard-Exits)
- Automatische Adaption der Spaltenbreite auf Anzahl der Statusphasen (im Template)
- umfangreiches Code-Refactoring

**Behobene Fehler**

- Cache-Verzeichnis wird absolut und nicht relativ eingebunden (ACHTUNG: Anpassung der Konfiguration erforderlich)

## Alpha 3 (19.11.2019)

**Neue Funktionen**

- Refresh der Cache-Daten forcieren

**Geänderte Funktionen**

- Cache-Verzeichnis wird relativ zum Hauptverzeichnis eingebunden

## Alpha 2 (09.11.2019)

**Neue Funktionen**

- Filterung nach Bearbeiter
- Filterung nach Projekt
- Konfiguration über separate json-Datei
- Visualisierung des Restaufwands über verschiedene Breiten
- Legende für Fälligkeit, Priorität und Restaufwand
- Favicon hinzugefügt

**Geänderte Funktionen**

- Verzeichnisstruktur verbessert
- Darstellung der Tickets hoher Prio verbessert

**Behobene Fehler**

- Berechnung (Rundung) der Resttage korrigiert
- Schriftgröße leicht angepasst zur besseren Darstellung unter Windows

## Alpha 1 (26.10.2019)

Erste Version mit grundlegenden Funktionen.

**Neue Funktionen**

- Verbindung zu Redmine via PHP Redmine API (https://github.com/kbsali/php-redmine-api/)
- Abfrage der Redmine-Tickets (inkl. Cache der Abfrage-Ergebnisse)
- Erstellung der Board-Darstellung
- Farbgebung für Dringlichkeit (abhängig vom Abgabedatum)
- Tooltip für Detailinformationen
- Klick zum Sprung direkt ins Ticket