# Vendor Verzeichnis

In diesem Verzeichnis werden über Composer externe Bibliotheken bereitgestellt.

Zur Installation von Composer diesen Schritten folgen:
https://getcomposer.org/download/

Danach im Hauptverzeichnis die notwendigen Bibliotheken installieren:
```php composer.phar install```

Bei Bedarf können die Bibliotheken auf folgende Weise aktualisiert werden:
```php composer.phar update```


