# tippy.js

Frontend Bibliothek zur Anzeige von Tooltips.

siehe auch:
* https://github.com/atomiks/tippyjs
* https://atomiks.github.io/tippyjs/

Hier lokal abgelegt um im Sinne des Datenschutzes (EU-DSGVO) externe Requests zu vermeiden.

Lizenz: MIT License (https://de.wikipedia.org/wiki/MIT-Lizenz)
