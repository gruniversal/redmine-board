var DynamicHighlight = {

    setup: function (search_field, search_elements, search_attribute, class_found, class_notfound) {

        // html id of input field to watch for search value
        this.search_field = search_field;

        // css class for elements to change
        this.search_elements = search_elements;

        // html attribute for elements to search in
        this.search_attribute = search_attribute;

        // css classes to add if attribute contains search value
        this.class_found = class_found;
        this.class_notfound = class_notfound;
    },

    run: function () {

        // init input delay
        this.search_timer = null;

        // listen to changes in the search field
        document.getElementById(this.search_field).addEventListener('input', this.trigger_input);

        // if field is not empty --> run once
        if (document.getElementById(this.search_field).value) {
            this.trigger_input();
        }
    },

    trigger_input: function () {
        // search uses a delay to prevent highlighting while still typing
        clearTimeout(this.search_timer);
        this.search_timer = setTimeout(DynamicHighlight.catch_timeout, 500);
    },

    catch_timeout: function () {
        // wrapper function to set scope to "this" (since "this" is not set with setTimeout)
        DynamicHighlight.highlight_elements();
    },

    highlight_elements: function () {

        this.search_term = document.getElementById(this.search_field).value;
        var foundElements = 0;

        document.querySelectorAll(this.search_elements).forEach(function (element) {
            var titleAttribute = element.getAttribute(this.search_attribute);
            if (titleAttribute && titleAttribute.toLowerCase().includes(this.search_term.toLowerCase())) {
                foundElements++;
                element.classList.add(this.class_found);
                element.classList.remove(this.class_notfound);
            } else {
                element.classList.add(this.class_notfound);
                element.classList.remove(this.found);
            }
        }.bind(this)); // "this" is bound here to allow use of "this" within the forEach function

        console.log("dynamic highlight: " + this.search_term + " (" + foundElements + ")");
    }
}