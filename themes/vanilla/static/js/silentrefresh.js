async function rb_silent_refresh() {

    // sleep is very important :D
    var sleep = function(ms) {
        return new Promise(
            resolve => setTimeout(resolve, ms)
        );
    }

    // build refresh url
    refresh_url = window.location.origin + window.location.pathname + "?silentrefresh";
    params = new URLSearchParams(window.location.search);
    if ( params.has('board') ) {
        refresh_url += "&board=" + params.get('board');
    }
    console.log( "silent refresh is active: " + refresh_url );

    // trigger silent refresh every 60s
    while ( true ) {

        await sleep(60000);

        start_spin_rotation();

        let response = await fetch( refresh_url );
        let text = await response.text();
        console.log( text );

        stop_spin_rotation();

        // turn refresh button green, when new data is available
        let count = parseInt( text );
        if ( count > 0 ) {
            let elem_button = document.querySelector(".refresh a");
            elem_button.style.background = '#66aa33';
        }
    }
}

function start_spin_rotation(){
    let elem_spin = document.querySelector(".refresh a .spin");
    elem_spin.classList.add("rotating");
}

function stop_spin_rotation(){
    let elem_spin = document.querySelector(".refresh a .spin");
    elem_spin.classList.remove("rotating");
}

rb_silent_refresh();
