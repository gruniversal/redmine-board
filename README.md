# Redmine Board

Diese PHP-Anwendung stellt Redmine-Tickets eines Kanban oder Scrum Workflows in einer globalen Übersicht dar.

Die Verbindung zu Redmine erfolgt über die PHP Redmine API von kbsali:<br />
https://github.com/kbsali/php-redmine-api/

Ich stelle diesen Code unter einer Non-Commercial Creative Commons Lizenz bereit:<br />
siehe [LICENSE.md](LICENSE.md).

Für kommerzielle Nutzung schreibt mir gern unter:<br />
gruniversal@gmx.de

Verbesserungen bitte ins GitLab Issue Board:<br />
https://gitlab.com/gruniversal/redmine-board/issues/

## Funktionen

- Abfrage aller Redmine-Tickets
	- Verbindung via PHP Redmine API
	- Abfrage der Ergebnisse (Einschränkung nach Trackertypen, Projekten und Bearbeitern möglich)
	- temporäres Caching der Abfrage-Ergebnisse
	- autom. Hintergrundaktualisierung der Daten über einen unsichtbaren Frontend-Request (einmal pro Minute)
- Erstellung der Board-Darstellung
	- Visualisierung aller Tickets als Karten einer Board-Matrix
	- Zeilen und Spalten frei anhand der verfügbaren Dimensionen definierbar (Standard: Projekte nach Trackertyp)
	- Filterung der Ergebnisse ebenfalls anhand der Dimensionen
	- verfügbare Dimensionen: Bearbeiter, Projekt, Kategorie, Ersteller, Status, Priorität und Typ
	- dynamische Highlight-Suche zur Hervorhebung von Tickets
	- Anpassung der Farbe, Rahmen und Breite anhand Ticketeigenschaften (z.B. der Fälligkeit oder Priorität)
	- Tooltip für Detailinformationen zu einem Ticket
	- Klick zum Sprung direkt ins Ticket
	- Automatische Adaption der Spaltenbreite auf Anzahl der Statusphasen
	- Support für eigene Themes sowie individuelle Templates über Board-Konfiguration
	- Custom Styles zur individuellen Hervorhebung von Tickets
- weitreichende Konfigurationsmöglichkeiten
	- multiple Boards konfigurierbar (inkl. Namen, Zeilen und Spalten sowie Filtermöglichkeiten)
	- autom. Datenanpassungen beim Import sowie Kalkulation auf importieren Feldern
	- Möglichkeit für übergreifende Boards: Angabe mehrerer Trackertypen, Projekte oder Bearbeiter gleichzeitig pro Board möglich
	- Trennung zwischen globaler Konfiguration und Board-Konfiguration (global nicht per Web zugreifbar)
	- zahlreiche Standardwerte um Konfiguration zu vereinfachen
	- Anzeige von individuellen Statistiken / KPIs über Konfiguration von Summen/Anzahlen und eigene Themes

Details siehe Konfiguration / Code.

## Anforderungen

Für die Anwendung sind folgende Anforderungen erforderlich:
- PHP >= 7.4 mit den Extensions cURL, SimpleXML und JSON
- einen Nutzer mit Redmine API, der Zugriff auf alle Redmine-Tickets hat.

Für die PHP Redmine API v2.2.0 gelten folgende Anforderungen:<br />
https://github.com/kbsali/php-redmine-api/blob/v2.2.0/README.md#requirements

Als Template-Engine kommt Twig v3.x zum Einsatz:<br />
https://twig.symfony.com/doc/3.x/intro.html#prerequisites

Für die Verwaltung der externen Bibliotheken nutze ich Composer:<br />
siehe [composer.json](composer.json).

## Installation

1. Quellcode herunterladen:<br />
https://gitlab.com/gruniversal/redmine-board/-/archive/master/redmine-board-master.zip

2. Composer im Hauptverzeichnis installieren:<br />
https://getcomposer.org/download/

3. externe Bibliotheken installieren:<br />
```php composer.phar install```

4. Globale Konfiguration anpassen:<br />
siehe [RedmineBoardGlobalConfig.php](./config/RedmineBoardGlobalConfig.php).

5. Board Konfiguration anpassen:<br />
siehe [boards.json](./config/boards.json).

6. ggf. Theme anpassen:<br />
siehe [THEMES.md](THEMES.md).

Eine Dokumentation der Konfiguration findet sich [hier](CONFIG.md).

## Letzte Änderungen

### Release v1.6.1 (17.02.2024)

**Geänderte Funktionen**

- dynamische Highlight-Suche: Groß-/Kleinschreibung ignorieren

### Release v1.6 (08.02.2024)

**Neue Funktionen**

- dynamische Highlight-Suche zur Hervorhebung von Tickets direkt im Frontend
- Custom Styles zur individuellen Hervorhebung von Tickets ergänzt (siehe [CONFIG.md](CONFIG.md)).

**Geänderte Funktionen**

- Theme "pluspol": Custom Style für Notfall-Tickets
- Theme "pluspol": Tippy lokal einbinden für besseren Datenschutz

**Behobene Fehler**

- dynamische Highlight-Suche: Highlighting nur für das Board / Legende nicht mit ausgrauen
- dynamische Highlight-Suche: initiale Suche, nur wenn es ein Suchwort gibt
- kleinere Code-Optimierungen

Alle veröffentlichten Versionen finden sich im [CHANGELOG](CHANGELOG.md).

## Roadmap

Im Moment soll die aktuelle Version erprobt und weiter optimiert werden.

Der Fokus liegt also auf Stabilisierung und Bugfixing.

Weitere Funktionen werden je nach Bedarf umgesetzt.

**Bekannte Fehler**

- Wenn man Filter-Parameter setzt, die nicht bekannt sind (z.B. wenn man eine bestimmte Ansicht bookmarkt, es aktuell aber keine Tickets für diese Auswahl gibt), wird der erste Eintrag im Filter (z.B. "Alle Bearbeiter") angezeigt, obwohl eine andere Auswahl getätigt wurde.
    - Hintergrund: Es werden für die Filter nur die Einträge gesammelt, für die es aktuell Tickets im Board gibt. Existiert kein Ticket für einen Bearbeiter, verschwindet er in der Ticketliste und es wird kein Eintrag bewusst ausgewählt.
    - Lösung 1: Man könnte die Einträge in der Filterliste über einen längeren Zeitraum speichern und aus dem Cache nutzen.
    - Lösung 2: Man könnte die unbekannte ID löschen und tatsächlich alle Einträge anzeigen.
    - Lösung 3: Man könnte einen zusätzlichen Eintrag "unbekannt" im Filter vornehmen.

**Sinnvolle Funktionserweiterungen**

- Konfiguration
	- Cronjob/Heartbeat zum Anheizen des Caches, Reminder und ggf. für Statistikfunktionen
	- "common" Konfiguration, die bei allen Boards geladen wird (z.B. common.json) um doppelte Einträge in den Boards vermeiden
	- Silent Refresh konfigurierbar machen (an/aus und Frequenz; ggf. pro Board)
- Board-Funktionen
	- Filterung nach Fälligkeit (insbesondere überfällige)
	- Filterung nach letzter Aktualisierung
	- Filterung nach Custom-Fields
- Auswertung / Statistik verbessern
	- Restaufwände nach Zeit: xh überfällig, xh diese Woche, xh nächste Woche, xh diesen Monat
	- (grafische) Statistik zum Ticket-Alter (Erstellung bzw. Aktualisierung)
	- Historische Auswertung (z.B. Gesamtanzahl, überfällige Tickets, etc.)

## Wunschliste

Die folgenden Themen lassen sich auf Grund fehlender Unterstützung der Redmine API im Moment nicht umsetzen.

**Auslesen des Feldes "Geschätzter Aufwand"**

- das Feld wird aus unbekannten Gründen über die Redmine API nicht bereitgestellt
- Update: offenbar ist dies erst ab Redmine v5.0.0 verfügbar: https://www.redmine.org/issues/34857
- Workaround: Nutzung eines Custom-Felds (z.B. "Restaufwand")

**Automatisches Auslesen der Status eines Trackertyps**

- zwar lassen sich mit `$this->redmine->issue_status->listing()` die verfügbaren Status und deren Sortierung auslesen, sowie mit `$this->redmine->tracker->listing()` die Liste der Trackertypen, es gibt aber keine Möglichkeit die Verknüpfung von beidem abzufragen
- Workaround: die entsprechende Information muss über die Konfiguration bereitgestellt werden
