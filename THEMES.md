# Redmine Board - Themes

Es gibt die Möglichkeit eigene Themes zu verwenden, diese liegen alle im Theme-Verzeichnis:

```

\themes\
|
+- vanilla		- Standard-Theme
|  |
|  +- static		- Verzeichnis für Bilder, CSS, Fonts, etc.
|  +- twig			- Template-Dateien
|
+- pluspol		- Beispiel-Theme
|  |
|  +- static		- Verzeichnis für Bilder, CSS, Fonts, etc.
|  +- twig			- Template-Dateien
|
+- ...

```

Das Verzeichnis muss über den Webserver erreichbar sein, um die statischen Inhalte laden zu können.

Grundsätzlich kann man den Ordner auch umbewegen. In diesem Fall ist die Konstante `REDMINEBOARD_THEMES_DIR` in der `index.php`anzupassen.

## Standard-Templates

Die Seitendarstellung wird aus folgenden Seitenbereichen zusammengebaut:

* `header` - HTML-Header
* `headline` - Überschrift und Boardauswahl
* `filter` - Filterfunktion
* `board` - Board-Matrix
* `card` - Darstellung eines Tickets
* `subline` - Anzahl und Zeitpunkt der Daten
* `custom` - gedacht für individuelle Erweiterung
* `footer` - HTML Footer

Im Standard werden dafür die gleichnamigen Templates aus dem Verzeichnis `twig` genutzt.

Die Templates basieren auf der Template-Engine [Twig v2](https://twig.symfony.com/doc/2.x/).

Innerhalb der Templates werden Variablen genutzt, die in [RedmineBoard](./src/RedmineBoard.php) definiert sind.

Für die Ticketdarstellung in `card` kann zudem über die Variable `issue` auf einzelne Werte des Tickets zugegriffen werden.

Auf [berechnete Felder](CONFIG.md#berechnungen) sowie deren [Anzahlen und Summen](CONFIG.md#anzahlen-und-summen) kann nur in `custom` zugegriffen werden.

## eigenes Theme erstellen

Um Anpassungen an der Gestaltung durchzuführen, sollte ein eigenes Theme erstellt werden.

Dazu kann einfach das Theme `vanilla` kopiert und nach eigenen Wünschen angepasst werden.

Hierzu können auch statische Ressourcen anderer Themes zur Reduzierung von Redundanzen verwendet werden. Beispielhaft ist dies im Theme `pluspol` dargestellt.

Die Template-Dateien werden immer nur aus dem Theme-Ordner eingebunden.

Welches Theme genutzt wird, wird in der [globalen Konfiguration](CONFIG.md#globale-konfiguration) festgelegt.

## Überschreibung einzelner Twigs

Es besteht die Möglichkeit einzelne Templates abhängig vom Board zu überschreiben.

Hierzu sind eine entsprechende [Template-Überschreibungen in der Board-Konfiguration](CONFIG.md#template-überschreibungen) erforderlich.
