<?php // phpcs:disable PSR1.Files.SideEffects.FoundWithSymbols

namespace RedmineBoard;

use \Gruniversal\Lib;

/**
 * Redmine Board - display a board of redmine issues
 *
 * @uses    PHP Redmine API (https://github.com/kbsali/php-redmine-api/)
 *
 * @see     https://gitlab.com/gruniversal/redmine-board
 * @license https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.de
 * @author  David Gruner (mailto:gruniversal@gmx.de / https://www.gruniversal.de)
 *
 */

/*****************************************************/
/** LIBS
/*****************************************************/

// display errors (only in DEV environment)
require_once __DIR__ . '/src/Lib/display_errors.php';

// define some functions if not defined by PHP8
require_once __DIR__ . '/src/Lib/polyfill-php8.php';

// autoload application classes and libraries
require_once __DIR__ . '/src/autoload.php';

// autoload external libraries (see: composer.json)
require_once __DIR__ . '/vendor/autoload.php';

/*****************************************************/
/** CONFIG
/*****************************************************/

// path to config --> setup your config here
define( 'REDMINEBOARD_CONFIG_DIR', __DIR__ . '/config/' );

// path to themes --> can be used for own themes (set in RedmineBoardGlobalConfig.php)
define( 'REDMINEBOARD_THEMES_DIR', __DIR__ . '/themes/' );

// load global configuration
require_once REDMINEBOARD_CONFIG_DIR . 'RedmineBoardGlobalConfig.php';

/*****************************************************/
/** EXECUTION
/*****************************************************/

// start time debugging
Lib\TimeDebug::init();

// run application
Main::run();

// show time debugging (only in DEV environment)
Lib\TimeDebug::set( 'done' );
Lib\TimeDebug::show();
